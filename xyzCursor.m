function xyzCursor(cursorOn, varargin)

    if ~exist('cursorOn', 'var')
        cursorOn = true;
    end
    
    fig = gcf;
    
    datacursormode on;
    dcm_obj = datacursormode(fig);
    set(dcm_obj, 'DisplayStyle', 'window');
    set(dcm_obj, 'Updatefcn', @psDataCursorCallback);
    
    if ~cursorOn
        datacursormode off;
    end
    
    h = zoom;
    set(h, 'rightclickaction', 'inversezoom')
    
    if isempty(varargin)
        set(h, 'ActionPostCallback', @psPostZoom);
    else
        set(h, 'ActionPostCallback', @psPostZoomMultiaxis);
    end
    
    function psPostZoom(obj, event_obj)
        %datetick('keeplimits');
    end

    function psPostZoomMultiaxis(obj, event_obj)
%         for m = 1:length(varargin)
%             datetick(varargin{m}, 'keeplimits');
%         end
    end
    
    function output_txt = psDataCursorCallback(obj, event_obj)
        
        pos = get(event_obj, 'Position');
        indx = get(event_obj, 'DataIndex');       
       
        output_txt = {['Y: ', num2str(pos(2), 8)], ...
                      ['X: ', num2str(pos(1), 8)], ...
                      ['Indx: ', num2str(indx, 12)]};
        
        if length(pos) > 2
            output_txt = [['Z: ' num2str(pos(3),8)], output_txt];        
        end
    end
end