function [Excel, Workbook] = xlswriteSelectionReportSheet(Excel, Workbook, sheetName, report)
%function [Excel, Workbook] = xlswriteRiskMatrix(Excel, Workbook, sheetName, portTable, portFig)

        % Do the interesting stuff: create, write, and format the new sheet  
        [Nr, Nc] = size(report);
        ExecuteWriteTable();
    
    
    %% Nested Functions =================================================================

    
    function ExecuteWriteTable()        
        sheetRange = xls.calcrange('A1', Nr, Nc);
        
        if Workbook.ReadOnly ~= 0
            %This means the file is probably open in another process.
            error('Workbok was opened in "ReadOnly" mode');
        end
        
        try
            [~, TargetSheet, visibility] = xls.activate_sheet(Excel, sheetName);
        catch
           error(message('MATLAB:xlswrite:InvalidSheetName', sheetName)); 
        end
        
        try
            Select(Range(Excel, sheetRange));
        catch
            error(message('MATLAB:xlswrite:SelectDataRange'));
        end

        % Export data to selected region.
        set(Excel.selection, 'Value', report);    
        ApplyTableFormat();
        xls.resetVisibility(TargetSheet, visibility);       
    end


    function ApplyTableFormat()
        
        Excel.Range('1:1').Font.Bold = true;
        Excel.Range('A:Z').EntireColumn.AutoFit;

        Excel.Range('2:2').Select;
        Excel.ActiveWindow.FreezePanes = true;   
        
        % Last column is "reason", and it's only non-empty for the winning row (at the top of each date block)
        ixBorders = find(~cellisempty(report(:,end)));
        ixBorders = ixBorders(2:end);  % don't draw around header row
        for m = 1:length(ixBorders)
            borderLeft = index_to_A1(ixBorders(m), 1);
            borderRange = xls.calcrange(borderLeft, 1, Nc);
            
            Excel.Range(borderRange).Select;
            border = Excel.Selection.Borders.Item(3);  % xlEddgeTop
            border.LineStyle = 1;  % xlContinuous
            border.Weight = 2;  % xlThin
        end        
        Excel.Range('A2').Select;   
    end

    %-------------------------------------------------------------------------------
    function cellA1 = index_to_A1(rowNum, colNum)
        cellA1 = sprintf('%s%d', xls.dec2base27(colNum), rowNum);
    end
end