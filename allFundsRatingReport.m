function report = allFundsRatingReport(PCSET, symbolSpec)

    ROTATION = PCSET.ROTATION;

    %%  Pass in the entire ROTATION struct as if it were a portfolio

    qpmInputs.RebalInterval = 'Y';    % Doesn't matter
    qpmInputs.OptionalsList = {};     % Doesn't matter
    qpmInputs.InvestmentHorizon = 3;  % Doesn't matter
    qpmInputs.RiskTolerance = 2;      % Doesn't matter
    qpmInputs.StartingCapital = 100000;  % Doesn't matter
    qpmInputs.MonthlySavings = 1000;     % Doesn't matter

    qpaInputs.StartDate = datenum(1996, 12, 31);  % Make start & end dates comparable to portfolio planning horizon
    qpaInputs.EndDate   = datenum(2016, 12, 31);
    qpaInputs.GuessOptionals = false;
    
    qpaInputs.YourPortfolio = ROTATION;
    qpaInputs.YourPortfolio.Weight = zeros(size(ROTATION.ISIN));

    qpaResult = qpaAPI(PCSET, symbolSpec, qpaInputs, qpmInputs);
    
    report = qpaResult.RatingReport;


end