function ix = downsampleMonthly(dates)

    [~, mo, dy] = datevec(dates(:));
    ix = [diff(mo) ~= 0; false];   % downsample, keeping dates at end of month
    if dy(1) <= 7       
        ix(1) = true;  % keep first day it's near the start of the month
    end
    
    if dy(end) >= 22
        ix(end) = true;  % keep last day if it's near the end of the month
    end
end