param = [];
param.StrategyName = 'QSE Selection Strategy';
param.Category = 'Skandiabanken';
param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
param.SymbolSpecFile = 'SB ROBO Universe 143 mapped.csv';
param.ResultsFile = 'Results QSE Selection Strategy.xlsx';
param.MarketDataCacheFile = 'SB ROBO running.mat';
param.CacheUpdateMode = 'AsNeeded';  % 'Offline', 'Force', 'AsNeeded'

param.MorningstarRatingPath = '.\Morningstar\Rating';
param.ExpenseRatioPath = '.\Morningstar\Net_Exp_Ratio';

param.StartDate = datenum(2013, 12, 31);
param.EndDate = datenum(2016, 12, 31);

param.StartCapital = 1000000;
param.HomeCurrency = 'NOK';
param.IndicatorCurrency = 'HOME';  % 'HOME', 'QUOTE'

param.ReallocatePeriod = 'S'; % 'W', 'M', 'Q', 'S', 'Y', 'None'
param.ReallocateDayOfWeek = '';  % 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'
param.EquityCalculations = 'D'; % 'D', 'W', 'M'

param.CommissionMinFixed = 0;
param.CommissionMaxPct = 0.0;   % this is not in percent??
param.CurrencySlippagePct = 0;

param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
param.DataGapsFillForward = 100;
param.SilentMode = false;

param.BenchmarkInstrument = 'Synthetic';

param.Selection.Group{1}.Name = 'Global Broad Equity';
param.Selection.Group{1}.Formula = 'Region=Global & Sector=Broad & InvestmentType=Equity';
param.Selection.Group{1}.Weight = 0.3;
param.Selection.Group{1}.Benchmark = 'NDDUWI Index';

param.Selection.Group{2}.Name = 'Global Fixed Income';
param.Selection.Group{2}.Formula = 'Region=Global & InvestmentType=Fixed Income';
param.Selection.Group{2}.Weight = 0.4;
param.Selection.Group{2}.Benchmark = 'LGCPTREH Index';

param.Selection.Group{3}.Name = 'European Technology Equity';
param.Selection.Group{3}.Formula = 'Region=Europe & Sector=Technology & InvestmentType=Equity';
param.Selection.Group{3}.Weight = 0.3;
param.Selection.Group{3}.Benchmark = 'SX8R Index';

param.Selection.TempNum = 1;

%param.QfPath = '..\..\..\sourcev2';  % path containing code for MVP2 Tool
param.QfPath = 'C:\Patrick\Career\Quantfolio\MATLAB\MVP2'; 


%% Execute the backtest

path(param.QfPath, path);

[state, bt, perf, symbolSpec, ...
 ROTATION, TIMING, REFERENCE, CCY, returnsTable] = RunStrategyBacktest(param);


