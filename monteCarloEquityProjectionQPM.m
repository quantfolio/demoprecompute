function mc = monteCarloEquityProjectionQPM(btDates, btEquity, startDate, Nyears, Nrealizations, forecastInterval, doPlots)
    % This function runs a Monte Carlo simulation of the future equity curve
    % of a strategy, by computing many possible realizations through bootstrap 
    % sampling (with replacement).  
    %
    % To use it, call it like: 
    %   mc = monteCarloEquityProjection(bt.runDate, bt.equity, true);
    %
    % This initial version has several hardcoded values which can be modified
    %   Nrealizations: the number of simulated future equity curves 
    %   Nyears: the number of years forward to project
    %   Percentiles: computes the 5th and 95th percentiles

    mc = struct;
            
    ixTradingDay = [false; diff(btEquity) ~= 0];
    btEquityTradingDay = btEquity(ixTradingDay);
    returns = btEquityTradingDay(2:end) ./ btEquityTradingDay(1:end-1) - 1;
    if isempty(returns) 
        return;
    end
    
    if any(~isfinite(returns))
        error('Encountered invalid value in btEquity causing a non-finite log return');
    end
    
    %figure; hist(logRet, 100); grid on;
    
    futDates = (startDate:1:btDates(end)+1+Nyears*365)';
    wkd = weekday(futDates);
    futDates = futDates(wkd > 1 & wkd < 7); % omit Sat and Sun
    % ToDo - also omit future holidays (requires holiday list or algorithm)
    
    eqSimMx = nan(length(futDates), Nrealizations);
    
    for m = 1:Nrealizations
        ix = randi(length(returns), length(futDates)-1, 1);
        returnSim = returns(ix);
%         eqSimMx(:,m) = [1; cumprod(exp(logRetSim))];
        eqSimMx(:,m) = [1; cumprod(1 + returnSim)];
    end
    
    tmp =  prctile(eqSimMx, [5, 50, 95], 2);

    if strcmpi(forecastInterval, 'M')
        [~, mo, dy] = datevec(futDates);
        ix = diff(mo) ~= 0;   % downsample
        if dy(end) > 20
            ix(end) = true;  % keep last day if it's near the end of the month
        end
    elseif strcmpi(forecastInterval, 'D')
        ix = true(size(futDates));
    else
        error('Unrecognized value for downsampleInterval');
    end
    
    mc.futDates = futDates(ix);
    mc.eqSimMx = eqSimMx(ix,:);
    mc.eqSim05 = tmp(ix, 1);
    mc.eqSimMed = tmp(ix, 2);
    mc.eqSim95 = tmp(ix, 3);       

    if doPlots
        figure; 
        plot(mc.futDates, mc.eqSimMx); datetick; grid on; hold on; timeCursor(false);
        title('Monte Carlo Projection of Future Equity');
        
        h95 = plot(mc.futDates, mc.eqSim95, 'color', [20, 20, 126]/255, 'LineWidth', 2.5); 
        h05 = plot(mc.futDates, mc.eqSim05, 'color', [20, 20, 126]/255, 'LineWidth', 2.5); 
        hM  = plot(mc.futDates, mc.eqSimMed, 'color', [255, 126, 20]/255, 'LineWidth', 2.5); 
        
        legend([h95, hM, h05], {'95th Percentile', 'Median', '5th Percentile'}, 'Location', 'NorthWest');
    end
end