function xlswriteSelectionReport(outPath, PCSET, REPORT, groupSpec, reallocPeriods)
   xlFilename = fullfile(outPath, sprintf('SelectionReport_%s.xlsx', datestr(PCSET.Timestamp, 'yyyy-mm-dd-HHMMSS')));
   [Excel, Workbook] = xls.create_for_writing(xlFilename);
   
   % Create Summary worksheet, listing the set of asset classes and selection rules
   fnames = fieldnames(groupSpec{1});
   fnames = fnames(~strcmpi(fnames, 'Ix'));
   summary = cell(length(groupSpec)+1, length(fnames));
   summary(1,:) = fnames;
   for g = 1:length(groupSpec)
      for nf = 1:length(fnames)
         summary{g+1, nf} = groupSpec{g}.(fnames{nf});
      end
   end
   xlswriteSelectionReportSheet(Excel, Workbook, 'Summary', summary);
   
   % Create a worksheet for each RebalancePeriod+AssetClass, showing what was selected and why
   for np = 1:length(reallocPeriods)
      origWarnState = warning('off', 'MATLAB:xlswrite:AddSheet');
      for g = 1:length(groupSpec)
         sheetName = sprintf('%s-%s', reallocPeriods{np}, groupSpec{g}.Name);
         report = REPORT.(reallocPeriods{np}){g};
         xlswriteSelectionReportSheet(Excel, Workbook, sheetName, report);
         fprintf('Wrote Selection Report Excel Sheet: %s\n', sheetName);
      end
      
      warning(origWarnState);
   end
   xls.save_and_close(Excel, Workbook);
   fprintf('Wrote Selection Report Excel File: %s\n', xlFilename);
   
end