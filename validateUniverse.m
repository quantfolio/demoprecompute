function [status, messages] = validateUniverse(universeFile, riskMetadataFile)

% 	* QA script for Universe files
% 		- Load Universe and RiskMetaData files
% 		- Check that all required column names exist in Universe
% 		- Check that all AssetClassCodes in RiskMetaData exist in Universe
% 			- Count how many instruments in each AssetClass 		
%       - Check that all rows in AssetClassCode and AssetClassName are 1-1 match
% 		- Check that rows with equal AssetClassCode also have equal AssetClassBroad
% 		- Check for duplicates in columns that should be unique:
% 			- Symbol, PerformanceId, FIGI, ISIN, Optional:(SecId, FundId, Sedol, Ticker)
% 		- Check that rows with RoboEligible==yes have valid data for required columns

%%
    status = true;
    messages = {};

    % Load Universe and RiskMetaData files ===================================
    if ~exist('universeFile', 'var') || isempty(universeFile)
        universeFile = '.\Universe\SB ROBO Universe 143 mapped PS5.csv';
    end
    if ~exist('riskMetadataFile', 'var') || isempty(riskMetadataFile)
        riskMetadataFile = '.\Universe\SB RiskMatrix Metadata.csv';
    end
    

    universe = importCsvGeneric(universeFile, ',', true, false);
    riskMatrixMetadata = importCsvGeneric(riskMetadataFile, ',', true, false);

    % Check that all required column names exist in Universe =================

    uFields = fieldnames(universe);
    rFields = fieldnames(riskMatrixMetadata);
    
    fieldsRequired = {'Symbol', 'PerformanceId', 'FIGI', 'ISIN', 'Instrument Name', 'BB Benchmark', ...
                      'Asset Class Code', 'Robo Eligible', 'GIFS Name', 'GIFS Code'};
    fieldsDesired = {'Currency', 'Exchange', 'Primary Prospectus Benchmark Id'};
    fieldsOptional = {'SecId', 'FundId', 'Sedol', 'Ticker', 'Domicile'};
    
    missingRequired = setdiff(strrep(fieldsRequired, ' ', ''), uFields);
    if ~isempty(missingRequired)
        str = '';
        for m = 1:length(missingRequired)
            str = [str, sprintf('"%s", ', missingRequired{m})];
        end
        messages{end+1, 1} = sprintf('ERROR: Universe file is missing required columns: %s', str(1:end-2));
        status = false;
    end
    
    missingDesired = setdiff(strrep(fieldsDesired, ' ', ''), uFields);
    if ~isempty(missingDesired)
        str = '';
        for m = 1:length(missingDesired)
            str = [str, sprintf('"%s", ', missingDesired{m})];
        end
        messages{end+1, 1} = sprintf('Warning: Universe file is missing desired columns: %s', str(1:end-2));        
    end
    
    missingOptional = setdiff(strrep(fieldsOptional, ' ', ''), uFields);
    if ~isempty(missingOptional)
        str = '';
        for m = 1:length(missingOptional)
            str = [str, sprintf('"%s", ', missingOptional{m})];
        end
        messages{end+1, 1} = sprintf('Information: Universe file is missing optional columns: %s', str(1:end-2));        
    end

    
    % Check that all AssetClassCodes in RiskMetaData exist in Universe =======
    % Count how many instruments in each AssetClass ==========================
    Nu = length(universe.Symbol);
    Nr = length(riskMatrixMetadata.Symbol);
    
    for m = 1:length(riskMatrixMetadata.Symbol)
        code = riskMatrixMetadata.AssetClassCode{m};
        ix = sum(strcmpi(universe.AssetClassCode, code));
        if ix == 0
            messages{end+1, 1} = sprintf('ERROR: Asset Class Code: %s is missing from Universe', code);
            status = false;
        end        
    end

    % Check for duplicates in columns that should be unique ==================
    requireUnique= [true,    true,            true,   false,  false,  false,    false,   false];
    checkFields = {'Symbol', 'PerformanceId', 'FIGI', 'ISIN', 'SecId', 'FundId', 'Sedol', 'Ticker'};
    checkFields = checkFields(ismember(checkFields, uFields));
    for nf = 1:length(checkFields)
        fname = checkFields{nf};
        uValues = unique(universe.(fname));
        for m = 1:length(uValues)
            str = uValues{m};
            num = sum(strcmpi(universe.(fname), str));
            if num > 1 && ~isempty(str)
                if requireUnique(nf)
                    messages{end+1, 1} = sprintf('ERROR: Found %d occurences of "%s" in column %s', num, str, fname);
                    status = false;
                else 
                    messages{end+1, 1} = sprintf('Warning: Found %d occurences of "%s" in column %s', num, str, fname);
                end
            end
        end
    end

    % Check that rows with RoboEligible==yes have valid data for required columns ========
    checkFields = strrep(fieldsRequired, ' ', '');
    checkFields = checkFields(ismember(checkFields, uFields));
    for m = 1:length(universe.(checkFields{1}))
        if strcmpi(universe.RoboEligible{m}, 'yes')
            for nf = 1:length(checkFields)
                fname = checkFields{nf};
                contents = strtrim(universe.(fname){m});
                if isempty(contents)
                    messages{end+1, 1} = sprintf('ERROR: Column %s is empty for Symbol: "%s" on row: %d', fname, universe.Symbol{m}, m+1);
                elseif ~isempty(strfind(contents, '#'))
                    messages{end+1, 1} = sprintf('ERROR: Column %s has unexpected value "%s" for Symbol: "%s" on row: %d', fname, contents, universe.Symbol{m}, m+1);                    
                end        
            end
        end
    end

    % Check for valid currency codes ===================================================
    ccyCodes = {'NOK', 'EUR', 'USD', 'SEK', 'GBP', 'JPY'};
    if ismember('Currency', uFields)
        for m = 1:length(universe.Currency)
            if ~ismember(universe.Currency{m}, ccyCodes)
                messages{end+1, 1} = sprintf('Warning: Found unexpected value: "%s" in Currency column on row %d', universe.Currency{m}, m+1);
            end
        end        
    end
    
    roboCount = sum(strcmpi(universe.RoboEligible, 'yes'));
    messages{end+1, 1} = sprintf('Information: Found %d instruments that are Robo Eligible', roboCount);
    
    
end 
