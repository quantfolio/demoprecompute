

pcFile = 'Output\Precompute_2017-06-02_005738.mat';

load(pcFile, 'PCSET', 'symbolSpec');

TI=1; OP=2; HI=3; LO=4; CL=5; VO=6; TR=7; VA=8;  % last two are Total Return and Valuation
ROTATION = PCSET.ROTATION;
REFERENCE = PCSET.REFERENCE;

startDate = datenum(1996,12,31);

%%

assetClassCode = 'c8';

ixG = find(strcmpi(symbolSpec.AssetClassCode, assetClassCode) & strcmpi(symbolSpec.RoboEligible, 'yes'));
GROUP = structSelect(ROTATION, ixG, 2);

uFBM = unique(GROUP.BBBenchmark);

ixS = find(ismember(upper(symbolSpec.Symbol), upper(uFBM)));
sGroup = structSelect(symbolSpec, ixS, 1)


%%

reallocPeriods = {'M', 'Q', 'Y'};

Ng = length(PCSET.M.Param);

nowStr = datestr(datetime('now','TimeZone', 'UTC'), 'yyyy-mm-dd_HHMMSS');
for r = 1:length(reallocPeriods)
    
    for g = 1:Ng
        period = reallocPeriods{r};
        assetClassCode = PCSET.(period).Param{g}.Selection.Group{1}.Name;
        equity  = PCSET.(period).Equity{g};
        runDate = PCSET.(period).RunDate{g};
        
        ixBM = find(strcmpi(PCSET.Risk.RiskMetaData.AssetClassCode, assetClassCode));
        assetClassName = PCSET.Risk.RiskMetaData.AssetClassName{ixBM};
        bmSymbol = PCSET.Risk.RiskMetaData.Symbol{ixBM};
        bars = PCSET.Risk.CellArrayOfBarsHomeCurrency{ixBM};
        ixT = find(bars(:,TI) >= startDate, 1, 'first');
        bars = bars(ixT:end,:);
        bmDate = bars(:, TI);
        bmEquity = bars(:,VA) / bars(1,VA);
        
        for m = 1:length(runDate)  % find first date that equity and bmEquity have in common
            ix1 = find(bmDate == runDate(m), 1, 'first');
            if ~isempty(ix1)
                break;
            end
        end        
        equity = equity / equity(m);
        bmEquity = bmEquity / bmEquity(ix1);
       
        % Plot QS equity vs. benchmark for AssetClass
        titleStr = sprintf('%s: %s, Period: %s', assetClassCode, assetClassName, period);
        [fig, ax] = plotLinked2('', '', titleStr, true);
        fig.Position(2) = fig.Position(2) + fig.Position(4) - 800;
        fig.Position(4) = 800;
        plot(ax(1), bmDate, bmEquity, '-', runDate, equity, '-'); 
        strBM = sprintf('AssetClass BM: %s', bmSymbol);
        legend(ax(1), {strBM, 'QS Equity'},  'location', 'NorthWest');
        title(ax(1), titleStr);
                
        % Plot fund-level benchmarks vs benchmark for AssetClass                
        ixFBM = find(strcmpi(symbolSpec.AssetClassCode, assetClassCode) & strcmpi(symbolSpec.RoboEligible, 'yes'));
        uFBM = setdiff(upper(unique(ROTATION.BBBenchmark(ixFBM))), upper(bmSymbol));
        ixR = find(ismember(upper(REFERENCE.Symbol), uFBM));
        if length(ixR) ~= length(uFBM)
            warning('ROTATION is missing benchmarks for AssetClass %s', assetClassCode);
            missingTickers = setdiff(uFBM, REFERENCE.Symbol);
            fprintf('  %s\n', missingTickers{:});
        end
        
        plt = plot(ax(2), bmDate, bmEquity); hold on;
        plt.LineWidth = 1.5;
        strLegend = {bmSymbol};
        for m = 1:length(ixR)
            bars = REFERENCE.BarsHomeCurrency{ixR(m)};
            if ~isempty(bars)
                ixT = find(bars(:,TI) >= startDate, 1, 'first');
                for n = 1:size(bars,1) % find first date that equity and bmEquity have in common
                    ix1 = find(bmDate == bars(n,TI), 1, 'first');
                    if ~isempty(ix1)
                        break;
                    end
                end                   
                fundBmEquity = bmEquity(ix1) * bars(ixT:end,VA) / bars(n,VA);
                fundBmDate = bars(ixT:end,TI);
                plot(ax(2), fundBmDate, fundBmEquity, '-');
                strLegend{end+1} = REFERENCE.Symbol{ixR(m)};
            end
        end
        tit = title(ax(2), sprintf('All benchmarks in %s', assetClassCode));
        tit.FontSize = 10; tit.FontWeight = 'normal';
        legend(strLegend, 'location', 'NorthWest');
        datetick(ax(1)); datetick(ax(2));
        grid(ax(1), 'on'); grid(ax(2), 'on');
        timeCursor(false, fig);
        set(fig, 'name', sprintf('%s-%s', period, assetClassCode));
        
        if doPdf  % Print a single PDF file
            filename = fullfile(pwd, sprintf('Plot_QsEquityVsBenchmarks_%s.pdf', nowStr));
            export_fig(filename, '-pdf', '-append', '-bookmark', '-nocrop', '-painters');
        end
        
    end
end

