function xlswriteDataDump(filename, riskMatrixMetadata, ROTATION)
    
    %% Create Excel file for writing
    [Excel, Workbook] = xls.create_for_writing(filename);
    
    ExecuteWrite();
    
    xls.save_and_close(Excel, Workbook); 
    
    %% Nested Functions =================================================================
    function ExecuteWrite()
        
        ValuationCol=8;
        
        for m = 1:length(ROTATION.Symbol)
            sheetName = riskMatrixMetadata.AssetClassCode{m};
            try
                xls.activate_sheet(Excel, sheetName);
            catch
                error(message('MATLAB:xlswrite:InvalidSheetName', sheetName));
            end
            bars = ROTATION.Bars{m};
            
            cellarray = cell(size(bars,1), 2);
            for n = 1:size(bars,1)
                cellarray{n,1} = datestr(bars(n,1), 'yyyy-mm-dd');
            end
            cellarray(:,2) = num2cell(bars(:,ValuationCol));
            [Nrow, Ncol] = size(cellarray);
            sheetRange = xls.calcrange('A1', Nrow, Ncol);
            
            
            if Workbook.ReadOnly ~= 0
                %This means the file is probably open in another process.
                error('Workbok was opened in "ReadOnly" mode');
            end
            
            try
                % select region.
                % Activate indicated worksheet.
                [~, TargetSheet, visibility] = xls.activate_sheet(Excel,sheetName);
            catch
                error(message('MATLAB:xlswrite:InvalidSheetName', sheetName));
            end
            
            try
                % Select range in worksheet.
                Select(Range(Excel, sheetRange));
            catch
                error(message('MATLAB:xlswrite:SelectDataRange'));
            end
            
            % Export data to selected region.
            set(Excel.selection, 'Value', cellarray);
            xls.resetVisibility(TargetSheet, visibility);
        end
    end
end