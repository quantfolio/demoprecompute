function xlswriteAllFundsRatingReport(outPath, PCSET, symbolSpec)
   % AllFundsRating report -------------------------------------------
   afrReport = allFundsRatingReport(PCSET, symbolSpec);
   
   outFileName = fullfile(outPath, sprintf('AllFundsRating_%s.xlsx', datestr(PCSET.Timestamp, 'yyyy-mm-dd-HHMMSS')));
   [Excel, Workbook] = xls.create_for_writing(outFileName);
   origWarnState = warning('off', 'MATLAB:xlswrite:AddSheet');
   sheetName = 'AllFundsRating';
   xlswriteAnalyzePortfolio(Excel, Workbook, sheetName, afrReport, [], [], []);
   xls.save_and_close(Excel, Workbook);
   warning(origWarnState);
   fprintf('Wrote AllFundsRating to Excel File: %s\n', outFileName);
end