

%% Import all data from disk

    param0 = param;  % cache it before we modify it
    param = parseStratParam(param);
    
    doLoad = ~exist('ROTATION', 'var')  || ~exist('TIMING', 'var') || ...
             ~exist('REFERENCE', 'var') || ~exist('CCY', 'var') || ~exist('symbolSpec', 'var') || ...
             isempty(ROTATION) || isempty(TIMING) || isempty(REFERENCE) || ...
             isempty(CCY) || isempty(symbolSpec);
    if doLoad
        [ROTATION, TIMING, REFERENCE, CCY, symbolSpec] = loadMarketData(param);
    end
    
%% Check that MorningstarRating & ExpenseRatio are available for instruments we care about


startDate = datenum(2013,12,31);
endDate = datenum(2016,12,31);
P = struct;
% Portfoio 1 ----------------------------------------------
m = 1;
P.Portfolio{m} = 'Portfolio 1';
P.Bucket{m} = 'Global Broad Equity';
P.RebalInterval{m} = '6 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'Global') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');
m = m+1;
P.Portfolio{m} = 'Portfolio 1';
P.Bucket{m} = 'Global Fixed Income';
P.RebalInterval{m} = '6 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'Global') & ...
          strcmpi(ROTATION.InvestmentType, 'Fixed Income');

m = m+1;
P.Portfolio{m} = 'Portfolio 1';
P.Bucket{m} = 'European Technology Equity';
P.RebalInterval{m} = '6 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'Europe') & ...
          strcmpi(ROTATION.Sector, 'Technology') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');    

% Portfoio 2 ----------------------------------------------          
m = m+1;
P.Portfolio{m} = 'Portfolio 2';
P.Bucket{m} = 'North American Broad Equity';
P.RebalInterval{m} = '1 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'North America') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');         

m = m+1;
P.Portfolio{m} = 'Portfolio 2';
P.Bucket{m} = 'North American Broad Fixed Income';
P.RebalInterval{m} = '1 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'North America') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Fixed Income');  
      
m = m+1;
P.Portfolio{m} = 'Portfolio 2';
P.Bucket{m} = 'North American Technology Equity';
P.RebalInterval{m} = '1 month';
P.Ix{m} = strcmpi(ROTATION.Region, 'North America') & ...
          strcmpi(ROTATION.Sector, 'Technology') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');


% Portfoio 3 ----------------------------------------------
m = m+1;
P.Portfolio{m} = 'Portfolio 3';
P.Bucket{m} = 'Global Energy';
P.RebalInterval{m} = '12 months';
P.Ix{m} = strcmpi(ROTATION.Region, 'Global') & ...
          strcmpi(ROTATION.Sector, 'Energy');
      
m = m+1;
P.Portfolio{m} = 'Portfolio 3';
P.Bucket{m} = 'European Broad Equity';
P.RebalInterval{m} = '12 months';
P.Ix{m} = strcmpi(ROTATION.Region, 'Europe') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');
      
m = m+1;
P.Portfolio{m} = 'Portfolio 3';
P.Bucket{m} = 'Global Broad Equity';
P.RebalInterval{m} = '12 months';
P.Ix{m} = strcmpi(ROTATION.Region, 'Global') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Equity');   

m = m+1;
P.Portfolio{m} = 'Portfolio 3';
P.Bucket{m} = 'Global Broad Fixed Income';
P.RebalInterval{m} = '12 months';
P.Ix{m} = strcmpi(ROTATION.Region, 'Global') & ...
          strcmpi(ROTATION.Sector, 'Broad') & ...
          strcmpi(ROTATION.InvestmentType, 'Fixed Income');         


%% Analysis ----------------------------------------------
      
Np = length(P.Portfolio);      
for m = 1:Np    
    portCheck = selectionDataCheck(ROTATION, P.Ix{m}, P.Portfolio{m}, P.Bucket{m}, ...
                                   P.RebalInterval{m}, startDate, endDate);
    P.PortCheck{m} = portCheck;
end

outFileName = 'Selection_Test_Data_Check.xlsx';

%%
for m = 1:length(P.PortCheck)
    sheetName = sprintf('Check%d', m);
    xlswrite(outFileName, P.PortCheck{m}, sheetName);
end



