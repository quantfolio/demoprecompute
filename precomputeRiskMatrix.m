function [RiskMatrixStruct, symbolSpec, riskMatrixMetadata, param, ...
          cellArrayOfBars, cellArrayOfBarsQuoteCurrency, cellArrayOfBarsHomeCurrency, success] = precomputeRiskMatrix(silentMode)
    % function [RiskMatrixStruct, symbolSpec, riskMatrixMetadata, ...
    %          cellArrayOfBars, cellArrayOfBarsQuoteCurrency, cellArrayOfBarsHomecurrency, success] = precomputeRiskMatrix(silentMode)
    % Note: the file referenced by param.RiskMatrixMetadata may contain simple symbols or 
    %       Synthetic benchmarks.  If synthetic, the "Symbol" column should contain
    %       something like:  {{'MXWO0IT Index'  0.7}  {'MXWO0IN Index' 0.3}}
    %       where NO commas are used, and spaces separate the elements.  It must be
    %       able to eval() to a valid cell array.

    if nargin == 0
        silentMode = true;
    end

    param = [];
    param.StrategyName = 'SB RiskMatrix';
    param.Category = 'SkandiaBanken';
    param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
    param.SymbolSpecFile = 'Universe\SB RiskMatrix Universe.csv';
    param.ResultsFile = 'Output\Results SB RiskMatrix.xlsx';
    param.MarketDataCacheFile = 'InputCache\SB ROBO running.mat';
    param.CacheUpdateMode = 'Offline';  % 'Offline', 'Force', 'AsNeeded'
    param.RiskMatrixMetadataFile = 'Universe\SB RiskMatrix Metadata.csv';

    param.LoadMorningstarData = false;

    param.StartDate = datenum(1996, 12, 20);  % be sure to get last trading day of Dec
    param.EndDate = datenum(2017, 1, 10);  % be sure to get first trading day of Jan
    % Note: We don't update the end date very often, for a few reasons:
    % 1) The RiskMatrix is based on 20 years of history, covering a wide range of market conditions
    % 2) The RiskMatrix is intended to be a long-term fairly stable object
    % 3) We don't want to reallocate customers' portfolios each period just because
    %    the RiskMatrix end date was changed

    param.StartCapital = 1000000;
    param.HomeCurrency = 'NOK';
    param.IndicatorCurrency = 'QUOTE';  % 'HOME', 'QUOTE'

    param.ReallocatePeriod = 'Y'; % 'W', 'M', 'Q', 'S', 'Y', 'None'
    param.ReallocateDayOfWeek = '';  % 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'
    param.EquityCalculations = 'D'; % 'D', 'W', 'M'

    param.CommissionMinFixed = 0;
    param.CommissionMaxPct = 0.0;   
    param.CurrencySlippagePct = 0;

    param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
    param.DataGapsFillForward = 100;
    param.SilentMode = true;
    param.BenchmarkInstrument = '';

    param.QfPath = '..';  % path containing code for QS Engine
    path(param.QfPath, path);

    %%

    param = parseStratParam(param);

    [ROTATION, ~, ~, ~, symbolSpec] = loadMarketData(param);
    
    riskMatrixMetadata = importCsvGeneric(param.RiskMatrixMetadataFile, ',', true, false);

    TI=1; OP=2; HI=3; LO=4; CL=5; VO=6; TR=7; VA=8;  % last two are Total Return and Valuation

    %%
    % ToDo: For testing, omit optionals.  Remove this when using Optionals !!!
    %riskMatrixMetadata = structSelect(riskMatrixMetadata, cellisempty(riskMatrixMetadata.Optional), 1); 
 
    Nac = length(riskMatrixMetadata.AssetClassCode);
    cellArrayOfBars = cell(Nac, 1);
    cellArrayOfBarsHomeCurrency = cell(Nac, 1);
    cellArrayOfBarsQuoteCurrency = cell(Nac, 1);
    for m = 1:Nac  % parse synthetic benchmarks or just pull the bars for the simple benchmarks 
        sym = riskMatrixMetadata.Symbol{m};
        braceCount = length(strfind(sym, '{'));  % a synthetic benchmark will have a list of symbols stored in a cell array, thus will contain braces
        
        if braceCount > 0  % if this asset class has a synthetic benchmark
            if braceCount ~= length(strfind(sym, '}')) 
                error('Symbol on row %d of %s has non-matching braces', m, param.RiskMatrixMetadataFile);
            end
            symList = eval(sym);
            bmSymbols = {};
            bmWeights = [];
            for n = 1:length(symList)
                bmSymbols{n} = symList{n}{1};
                bmWeights(n) = symList{n}{2};
            end
            [Bars, BarsHomeCurrency] = computeSyntheticBenchmark(ROTATION, bmSymbols, bmWeights);           
        else  % a simple index matches this asset class
            ix = find(strcmpi(sym, ROTATION.Symbol));
            if length(ix) ~= 1
                error('Expected exactly one match for symbol "%s", found %d', sym, length(ix));
            end
            Bars = ROTATION.Bars{ix};
            BarsHomeCurrency = ROTATION.BarsHomeCurrency{ix};
        end
        
        if isempty(riskMatrixMetadata.IndicatorCurrency{m})
            indicatorCurrency = param.IndicatorCurrency;
        elseif strcmpi(riskMatrixMetadata.IndicatorCurrency{m}, 'HOME')
            indicatorCurrency = 'HOME';
        elseif strcmpi(riskMatrixMetadata.IndicatorCurrency{m}, 'QUOTE')
            indicatorCurrency = 'QUOTE';
        else
            error('Unrecognized value for riskMatrixMetadata.IndicatorCurrency{%d}', m);
        end
        
        if strcmpi(indicatorCurrency, 'HOME')
            cellArrayOfBars{m} = BarsHomeCurrency;
        elseif strcmpi(indicatorCurrency, 'QUOTE')
            cellArrayOfBars{m} = Bars;
        else
            error('Unrecognized value for param.IndicatorCurrency');
        end
        cellArrayOfBarsHomeCurrency{m} = BarsHomeCurrency;
        cellArrayOfBarsQuoteCurrency{m} = Bars; 
    end
    
    RET = struct;
    for m = 1:Nac
        dates0 = cellArrayOfBars{m}(:,TI);
        ix = dates0 >= param.StartDate & dates0 <= param.EndDate;
        dates = dates0(ix);
        prices = cellArrayOfBars{m}(ix, VA);
        nPerYear = length(dates) / ((dates(end) - dates(1)) / 365);
        logret = diff(log(prices));
        RET.Dates{m} = dates;
        RET.Prices{m} = prices;
        RET.Mean(m) = mean(logret) * nPerYear;
        RET.StdDev(m) = std(logret) * sqrt(nPerYear);

        ix = logret < 0;
        if sum(ix) > 0
            downsideLogret = zeros(size(logret));
            downsideLogret(ix) = logret(ix);
            RET.DownsideDev(m) = sqrt(sum(downsideLogret .^ 2) / length(downsideLogret)) * sqrt(252);
        else
            RET.DownsideDev(m) = 0;
        end
    end

    [covMx, corMx, ~, sigmaVec, retVec, logRetVec, ~, yearsHistory] = monthlyCov(cellArrayOfBars, VA, param.StartDate, param.EndDate);

    managementFeePct = 0.4 / 100;  % 40 basis points per year as per Skandiabanken meeting 2017-05-26
    logRetVec = logRetVec - managementFeePct;  % annualized return
    
    corMxTab = cell(Nac+1, Nac+1);
    corMxTab(2:end,1) = riskMatrixMetadata.AssetClassCode;
    corMxTab(1,end-Nac+1:end) = riskMatrixMetadata.AssetClassCode';
    corMxTab(2:end, 2:end) = num2cell(corMx);
    
    if ~silentMode
        % figure; plot(1:Ns, RET.StdDev, 1:Ns, sigmaVec); xlabel('Instrument'); ylabel('\sigma'); 
        %         grid on; legend('Daily (or max available)', 'Monthly');
        %         title('Sigma of Returns, computed two ways');

        figure; plot( 1:Nac, logRetVec, 1:Nac, RET.Mean, 1:Nac, sigmaVec, 1:Nac, RET.StdDev); xlabel('Instrument'); grid on; 
                legend('Mean Return (Monthly)', 'Mean Return (Daily)', 'StdDev Return (Monthly)', 'StdDev Return (Daily)', 'Location', 'NorthWest');

        figure; 
        for m = 1:length(RET.Dates)
            plot(RET.Dates{m}, RET.Prices{m} / RET.Prices{m}(1)); hold on;
        end
        datetick; grid on; legend(riskMatrixMetadata.AssetClassCode, 'Location', 'NorthWest');
        title('Asset Class Performance'); xyzCursor(false);
    end
        
%% A single target portfolio

% sigmaTarget = .10;
% tic;
% [w, mu, sigma]  = portOpt(RET.Mean(:), covMx, sigmaTarget);
% toc
% 
%         
% figure; plot(sigmaVec, RET.Mean, '.', sigma, mu, 'ro'); grid on; xlabel('\sigma'); ylabel('Return');
%         legend('Instruments', 'Optimal Portfolio'); xyzCursor(false);

%% Efficient frontier - Unconstrained

%   sigmaTargetVec = (1:12)*15/12 / 100;  % used below in constrained portfolios: risk in 12 portfolios ranges from 0 to 10%

%   sigmaTargetVec = [ 0.013 	 0.020 	 0.070 	 0.050 	 0.070 	 0.120 	 0.10 	 0.12 	 0.17 	 0.14 	 0.17 0.20 ];
%   sigmaTargetVec = [0.013	0.02	0.07	0.05	0.07	0.12	0.09	0.11	0.155	0.14	0.17	0.22];
%   sigmaTargetVec = [0.01 0.02 0.045	0.061	0.076	0.107	0.087	0.109	0.153	0.123	0.165	0.235];
    sigmaTargetVec = [0.01 0.02 0.045	0.061	0.076	0.107	0.087	0.109	0.153	0.13	0.165	0.235];

    %riskVecTarg = [0, 0.005, (0.01:.01:.25)]';
    riskVecTarg = unique([0; 0.005; sigmaVec(:); sigmaTargetVec(:); (round(100*sigmaTargetVec(end))/100:.01:0.25)' ]');
    weightMx = zeros(Nac, length(riskVecTarg));
    returnVecFront = zeros(size(riskVecTarg));
    riskVecFront = zeros(size(riskVecTarg));
    for r = 1:length(riskVecTarg)
        [w, mu, sigma]  = portOpt(logRetVec(:), covMx, riskVecTarg(r), [], [], [], [], []);
        weightMx(:,r) = w;   
        returnVecFront(r) = mu;
        riskVecFront(r) = sigma;
    end

    sharpeVecFront = returnVecFront ./ riskVecFront;

%     figure; axEF(1) = subplot(2,1,1);
%             plot(sigmaVec, RET.Mean, '.', riskVecFront, returnVecFront, '-'); grid on; 
%             xlabel('\sigma'); ylabel('Return'); 
%             legend('Instruments', 'Efficient Frontier', 'Location', 'SouthEast');
% 
%             axEF(2) = subplot(2,1,2);
%             plot(riskVecFront, sharpeVecFront); grid on; 
%             xlabel('\sigma'); ylabel('Return/Risk Ratio'); xyzCursor(false);
%             
%             dX = diff(axEF(1).XLim) * 0.01;
%             dY = diff(axEF(1).YLim) * 0.01;
%             hTxt = text(axEF(1), sigmaVec+dX, RET.Mean-dY, riskMatrixMetadata.AssetClassCode, 'FontSize', 9);
            
%     figure; ax = gca;
%             plot(sigmaVec, RET.Mean, '.', riskVecFront, returnVecFront, '-'); grid on; 
%             xlabel('\sigma'); ylabel('Return'); 
%             legend('Instruments', 'Efficient Frontier', 'Location', 'SouthEast');
%             dX = diff(ax.XLim) * 0.01;
%             dY = diff(ax.YLim) * 0.01;
%             hTxt = text(ax, sigmaVec+dX, RET.Mean-dY, riskMatrixMetadata.AssetClassCode, 'FontSize', 9);
%             title('Unconstrained Efficient Frontier')

%% Risk Matrix - Constrained
% Iterate over all possible combinations of optional portfolios
    
    if ~silentMode
        origWarnState = warning('off', 'MATLAB:xlswrite:AddSheet');
        timeStr = datestr(datetime('now','TimeZone', 'UTC'), 'yyyy-mm-dd_HHMMSS');
        dumpFileName = sprintf('TimeSeriesDataDump_%s.xlsx', timeStr);
        dumpFileName = fullfile(pwd, 'Output', dumpFileName);
        xlswriteDataDump(dumpFileName, riskMatrixMetadata, ROTATION);

        % Create Excel file and open it for repeated writing - OUTSIDE the loop!
        filename = sprintf('RiskMatrixReport_%s.xlsx', timeStr);
        filename = fullfile(pwd, 'Output', filename);
        [Excel, Workbook] = xls.create_for_writing(filename);     
    end
    
    ixOptional = ~cellisempty(riskMatrixMetadata.Optional);
    fxOptional = find(ixOptional);
    N = length(fxOptional);
    [permuteMx, permuteInt] = permuteOptions(N);
    Nopt = length(permuteInt);  % Number of optional portfolios to be optimized
    
    RiskMatrixStruct = struct;
    RiskMatrixStruct.PermuteInt = permuteInt;
    RiskMatrixStruct.PermuteMx = permuteMx;
    RiskMatrixStruct.RiskMatrix = cell(Nopt,1);
    RiskMatrixStruct.UnconstrainedFrontierReturn = returnVecFront;
    RiskMatrixStruct.UnconstrainedFrontierRisk  = riskVecFront;
    RiskMatrixStruct.ConstrainedFrontierReturn = cell(Nopt, 1);
    RiskMatrixStruct.ConstrainedFrontierRisk  = cell(Nopt, 1);
    RiskMatrixStruct.RiskMatrixReport = cell(Nopt,1);
    
    for m = 1:Nopt        
        equityMaxVec   = [0 0 10 20 30 40 50 60 70 60 75 100] / 100;
    %     equityMaxVecC6 = [15, 15, 15, 15, 15, 15, 15, 15, 15, 16, 18, 20] / 100;  % Set manual max for Home Market, b/c Norway is a small market
        equityMinVec = zeros(size(equityMaxVec));
        maxVecC6  = repmat(0.15, size(equityMaxVec));  % set manual Weight max for this asset class
        maxVecC21 = repmat(0.15, size(equityMaxVec));  % set manual Weight max for this asset class

        isEquity = strcmpi(riskMatrixMetadata.AssetClassBroad, 'Equity');
        ixC6 = find(strcmpi(riskMatrixMetadata.AssetClassCode, 'c6'));
        ixC9 = find(strcmpi(riskMatrixMetadata.AssetClassCode, 'c9'));
        ixC21 = find(strcmpi(riskMatrixMetadata.AssetClassCode, 'c21'));
        
        ixSelected = false(size(ixOptional));
        ixSelected(fxOptional(permuteMx(m,:))) = true;

        Nrt = 3;  % risk tolerance
        Nih = 4;  % investment horizon
        Np = Nrt * Nih;  % Number of portfolios
        riskWeightMx = zeros(Nac, Np);  % Row for each symbol, col for each risk level
        portReturn = zeros(1, Np);
        portSigma = zeros(1, Np);
        ixOK = true(1, Np);

        minWeight = 0.05;
        maxWeight = 0.25;
        dynamicRangeTarget = 5.0;  % ratio of largest to smallest weight should be at least this amount

        for ih = 1:Nih
            for rt = 1:Nrt
                col = Nrt * (ih - 1) + rt;
                ssField = sprintf('Risk%d', col);

                indicatorVec = strcmp(riskMatrixMetadata.(ssField), '1') | ixSelected;
                lb = zeros(size(indicatorVec));
                ub = zeros(size(indicatorVec));

                % Bounds on Equity instruments ----------------
                weightToAllocate = equityMaxVec(col);
                ixEq = indicatorVec & isEquity;
                numToAllocateEq = sum(ixEq);
                aveWeight = weightToAllocate / numToAllocateEq;
                aveWeight(isnan(aveWeight)) = 0;
                minWeightThis = weightToAllocate / (numToAllocateEq - 1 + dynamicRangeTarget);
                minWeightThis = max(min(minWeightThis, 0.05), .01);  % Cap min weight between 1% and 5%
                minWeightThis = min(minWeightThis, aveWeight);  % 
                maxWeightThis = 1.0 - (numToAllocateEq-1) * minWeightThis;
                maxWeightThis = min(maxWeight, maxWeightThis);
 
                lb(ixEq) = minWeightThis;
                lb(ixC9) = 2 * lb(ixC9);  % if Global Developed is participating, give it double the minimum allocation
                ub(ixEq) =  maxWeightThis;
                ub(ixEq & ixOptional) = min(ub(ixEq & ixOptional), 0.15);  % Limit the optionals to 15%
                ub(ixC6) = maxVecC6(col);  %#ok<*FNDSB> % Kludgy way to constrain Home Market equity to 15%
                
                % Bounds on Fixed-Income & Cash instruments ----------------
                weightToAllocate = 1 - equityMaxVec(col);
                ixFi = indicatorVec & ~isEquity;
                numToAllocate = sum(ixFi);
                aveWeight = weightToAllocate / numToAllocate;
                aveWeight(isnan(aveWeight)) = 0;
                %minWeightThis = min(minWeight, aveWeight / sqrt(5));
                minWeightThis = minWeight;
                maxWeightThis = 1.0 - (numToAllocate-1) * minWeightThis;
                maxWeightThis = max(maxWeight, min(aveWeight * sqrt(5), maxWeightThis)); 

                lb(ixFi) = minWeightThis;
                ub(ixFi) =  maxWeightThis;
                ub(ixC21) = min(ub(ixC21), maxVecC21(col));

                sigmaTarget = sigmaTargetVec(col);
                [w, mu, sigma] = portOpt(logRetVec(:), covMx, sigmaTarget, lb, ub, equityMinVec(col), equityMaxVec(col), isEquity);

                if any(isnan(w)) || abs(1 - sum(w)) > 1.0e-6
                    ixOK(col) = false;
                else
                    w = round(w * 1000) / 1000;  % round to the nearest 0.1%
                    if sum(w) ~= 1.0
                        [~, ixMax] = max(w);
                        w(ixMax) = w(ixMax) - (sum(w) - 1);  % let the largest weight absorb the rounding error
                    end
                end           

                riskWeightMx(:,col) = w;
                portReturn(col) = mu;
                portSigma(col) = sigma;
                equityAllocated(col) = sum(w(isEquity));
            end
        end
        
        RiskMatrixStruct.RiskMatrix{m} = riskWeightMx;
        RiskMatrixStruct.ConstrainedFrontierReturn{m} = portReturn;
        RiskMatrixStruct.ConstrainedFrontierRisk{m}  = portSigma;
        
        % Create a human-friendly table of results 
        riskMatrixReport = cell(Nac+11, Np+6);  % cell array pre-loaded with empty strings
        riskMatrixReport{1,1} = sprintf('CONSTRAINED OPTIMAL PORTFOLIOS:  Currency=%s', param.IndicatorCurrency);
        riskMatrixReport(3,:) = {'Broad Asset Class', 'Asset Class Name', 'Asset Class Code', 'Years History', 'Historical Return', 'Historical Risk', 'Low', 'Med', 'High', 'Low', 'Med', 'High', 'Low', 'Med', 'High', 'Low', 'Med', 'High'};
        for rr = 1:Nac
            riskMatrixReport(rr+3,:) = [riskMatrixMetadata.AssetClassBroad{rr}, ...
                              riskMatrixMetadata.AssetClassName{rr}, ...
                              riskMatrixMetadata.AssetClassCode{rr}, ...
                              round(yearsHistory(rr) * 1000)/1000, ...
                              round(logRetVec(rr) * 1000)/1000, ...
                              round(sigmaVec(rr) * 1000)/1000, ...
                              num2cell(riskWeightMx(rr,:))];
        end
        rr = Nac + 4;
        riskMatrixReport(rr+1, end-Np:end) = ['Equity Min Constraint:', num2cell(equityMinVec)];
        riskMatrixReport(rr+2, end-Np:end) = ['Equity Max Constraint:', num2cell(equityMaxVec)];
        riskMatrixReport(rr+3, end-Np:end) = ['Portfolio Equity:', num2cell(equityAllocated)];

        riskMatrixReport(rr+5, end-Np:end) = ['Risk Constraint:', num2cell(sigmaTargetVec)];
        riskMatrixReport(rr+6, end-Np:end) = ['Portfolio Expected Risk:', num2cell(round(portSigma * 1000)/1000)];
        riskMatrixReport(rr+7, end-Np:end) = ['Portfolio Expected Return:', num2cell(round(portReturn * 1000)/1000)];

        Nc = size(corMxTab, 1);
        riskMatrixReport = [riskMatrixReport, cell(size(riskMatrixReport,1), 2 + size(corMxTab, 2))];
        riskMatrixReport(3:3+Nc-1, end-Nc+1:end) = corMxTab;
        riskMatrixReport{1, end-Nc+1} = 'CORRELATION MATRIX';
        
        RiskMatrixStruct.RiskMatrixReport{m} = riskMatrixReport;
        
        if ~silentMode
            portFig = figure; 
            plt = plot(portSigma, portReturn, '-', riskVecFront, returnVecFront, '-', sigmaVec, logRetVec, 'g.'); grid on;
            legend('Constrained Portfolios', 'Unconstrained Frontier', 'Asset Classes', 'Location', 'NorthWest');
            xlabel('\sigma'); ylabel('Return'); xyzCursor(false); 
            plt(1).Marker = '.';
            ax = gca;
            %ax.XLim = [0, round(100 * max(sigmaTargetVec)) / 100];
            dX = diff(ax.XLim) * 0.01;
            dY = diff(ax.YLim) * 0.01;
            hTxt = text(ax, sigmaVec+dX, logRetVec-dY, riskMatrixMetadata.AssetClassCode, 'FontSize', 9);
            title(sprintf('Efficient Frontier: RiskMatrix# %d', permuteInt(m)));

            sheetName = sprintf('RM%d', permuteInt(m));
            xlswriteRiskMatrixSheet(Excel, Workbook, sheetName, riskMatrixReport, portFig);
            %fprintf('RiskMatrix: Wrote Excel Sheet: %s\n', sheetName);
            close all;
        end
        fprintf('RiskMatrix: Computed %d of %d\n', m, Nopt);   
    end
    
    if ~silentMode
        % Save Excel file and delete all excel objects
        xls.save_and_close(Excel, Workbook); 
        warning(origWarnState);
        fprintf('RiskMatrix: Completed writing Excel File: %s\n', filename);
    end
    
    %%
    success = true;
    if any(~ixOK)
        success = false;
    end 
