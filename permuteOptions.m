function [permuteMx, permuteInt] = permuteOptions(N)
    % Finds all the ways of choosing 0, 1, 2, 3, or 4 items from a set of N
    % availble options.  N must be greater than 4.
    
    permuteMx = false(1,N);  % N Choose 0
    permuteMx = [permuteMx;permutationVectors(N,1)];
    permuteMx = [permuteMx;permutationVectors(N,2)];
    permuteMx = [permuteMx;permutationVectors(N,3)];
    permuteMx = [permuteMx;permutationVectors(N,4)];
      
    Len = size(permuteMx,1);
    permuteInt = nan(Len, 1);
    pow2vec = 2 .^ (N-1:-1:0);
    for m = 1:Len
        permuteInt(m) =  sum(permuteMx(m,:) .* pow2vec);
    end
    [permuteInt, ix] = sort(permuteInt);
    permuteMx = permuteMx(ix,:);
end
    
function vectors = permutationVectors(N,k)
    if k > N
        vectors = false(0,N);
        return;
    end
    picks = nchoosek(1:N,k);
    [r,~] = size(picks); 
    vectors = false(r,N);
    for i=r:-1:1
        vec = false(1,N);
        vec(picks(i,:))= true;
        vectors(i,:) = vec;
    end
end