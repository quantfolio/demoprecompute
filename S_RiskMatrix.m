param = [];
param.StrategyName = 'SB RiskMatrix';
param.Category = 'SkandiaBanken';
param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
param.SymbolSpecFile = 'SB AssetClass SymbolSpec.csv';
param.ResultsFile = 'Results SB RiskMatrix.xlsx';
param.MarketDataCacheFile = 'SB ROBO running.mat';
param.CacheUpdateMode = 'Offline';  % 'Offline', 'Force', 'AsNeeded'

param.MorningstarRatingPath = '';
param.ExpenseRatioPath = '';
param.FundSizePath = '';

param.StartDate = datenum(2005, 12, 31);
param.EndDate = datenum(2016, 12, 31);

param.StartCapital = 1000000;
param.HomeCurrency = 'NOK';
param.IndicatorCurrency = 'HOME';  % 'HOME', 'QUOTE'

param.ReallocatePeriod = 'Y'; % 'W', 'M', 'Q', 'S', 'Y', 'None'
param.ReallocateDayOfWeek = '';  % 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'
param.EquityCalculations = 'D'; % 'D', 'W', 'M'

param.CommissionMinFixed = 0;
param.CommissionMaxPct = 0.0;   
param.CurrencySlippagePct = 0;

param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
param.DataGapsFillForward = 100;
param.SilentMode = true;
param.BenchmarkInstrument = '';


param.QfPath = '..';  % path containing code for QS Engine
path(param.QfPath, path);

%%

param = parseStratParam(param);

[ROTATION, TIMING, REFERENCE, CCY, symbolSpec] = loadMarketData(param);

TI=1; OP=2; HI=3; LO=4; CL=5; VO=6; TR=7; VA=8;  % last two are Total Return and Valuation

%%



Ns = length(ROTATION.Symbol);
RET = struct;
for m = 1:length(ROTATION.Symbol)
    dates0 = ROTATION.Bars{m}(:,TI);
    ix = dates0 >= param.StartDate & dates0 <= param.EndDate;
    dates = dates0(ix);
    prices = ROTATION.BarsHomeCurrency{m}(ix, VA);
    nPerYear = length(dates) / ((dates(end) - dates(1)) / 365);
    logret = diff(log(prices));
    RET.Dates{m} = dates;
    RET.Prices{m} = prices;
    RET.Mean(m) = mean(logret) * nPerYear;
    RET.StdDev(m) = std(logret) * sqrt(nPerYear);
    
    ix = logret < 0;
    if sum(ix) > 0
        downsideLogret = zeros(size(logret));
        downsideLogret(ix) = logret(ix);
        RET.DownsideDev(m) = sqrt(sum(downsideLogret .^ 2) / length(downsideLogret)) * sqrt(252);
    else
        RET.DownsideDev(m) = 0;
    end
end

cellarrayofbars = ROTATION.BarsHomeCurrency;
[covMx, corMx, numObs, sigmaVec] = monthlyCov(cellarrayofbars, VA, param.StartDate, param.EndDate);

% figure; plot(1:Ns, RET.StdDev, 1:Ns, sigmaVec); xlabel('Instrument'); ylabel('\sigma'); 
%         grid on; legend('Daily (or max available)', 'Monthly');
%         title('Sigma of Returns, computed two ways');

figure; plot(1:Ns, RET.Mean, 1:Ns, sigmaVec, 1:Ns, RET.StdDev); xlabel('Instrument'); grid on; 
        legend('Mean Return', 'StdDev Return (Monthly)', 'StdDev Return (Daily)');
        
figure; 
for m = 1:length(RET.Dates)
    plot(RET.Dates{m}, RET.Prices{m} / RET.Prices{m}(1)); hold on;
end
datetick; grid on; legend(ROTATION.Classification, 'Location', 'NorthWest');
title('Asset Class Performance'); xyzCursor(false);
        
%% A single target portfolio

% sigmaTarget = .10;
% tic;
% [w, mu, sigma]  = portOpt(RET.Mean(:), covMx, sigmaTarget);
% toc
% 
%         
% figure; plot(sigmaVec, RET.Mean, '.', sigma, mu, 'ro'); grid on; xlabel('\sigma'); ylabel('Return');
%         legend('Instruments', 'Optimal Portfolio'); xyzCursor(false);


%% Efficient frontier - Unconstrained

tic;
riskVecTarg = [0, 0.005, (0.01:.01:.25)]';
weightMx = zeros(Ns, length(riskVecTarg));
returnVecFront = zeros(size(riskVecTarg));
riskVecFront = zeros(size(riskVecTarg));
for r = 1:length(riskVecTarg)
    [w, mu, sigma]  = portOpt(RET.Mean(:), covMx, riskVecTarg(r));
    weightMx(:,r) = w;   
    returnVecFront(r) = mu;
    riskVecFront(r) = sigma;
end
toc

sharpeVecFront = returnVecFront ./ riskVecFront;

figure; subplot(2,1,1);
        plot(sigmaVec, RET.Mean, '.', riskVecFront, returnVecFront, '-'); grid on; 
        xlabel('\sigma'); ylabel('Return'); 
        legend('Instruments', 'Efficient Frontier', 'Location', 'SouthEast');

        subplot(2,1,2);
        plot(riskVecFront, sharpeVecFront); grid on; 
        xlabel('\sigma'); ylabel('Return/Risk Ratio'); xyzCursor(false);

%% Risk Matrix - Constrained

equityWeightVec = [0 0 10 20 30 40 50 60 70 80 90 100] / 100;
sigmaTargetVec = (1:12)*10/12 / 100;

isEquity = double(strcmpi(symbolSpec.BroadAssetClass, 'Equity'));
isEquity = isEquity(symbolSpec.Rotation);  % ignore non-tradeable rows

Nrt = 3;  % risk tolerance
Nih = 4;  % investment horizon
Nr = Nrt * Nih;
riskWeightMx = zeros(Ns, Nr);  % Row for each symbol, col for each risk level
portReturn = zeros(1, Nr);
portSigma = zeros(1, Nr);

minWeight = 0.05;
maxWeight = 0.35;
bounds = nan(4,Nr);


for ih = 1:Nih
    for rt = 1:Nrt
        col = Nrt * (ih - 1) + rt;
        ssField = sprintf('Risk%d', col);
        
        indicatorVec = strcmp(symbolSpec.(ssField), '1');
        indicatorVec = indicatorVec(symbolSpec.Rotation);
        lb = zeros(size(indicatorVec));
        ub = zeros(size(indicatorVec));
        
        % Bounds on Equity instruments ----------------
        weightToAllocate = equityWeightVec(col);
        ixEq = indicatorVec & isEquity > 0;
        numToAllocateEq = sum(ixEq);
        aveWeight = weightToAllocate / numToAllocateEq;
        aveWeight(isnan(aveWeight)) = 0;
        minWeightThis = min(minWeight, aveWeight / sqrt(5));
        maxWeightThis = 1.0 - (numToAllocateEq-1) * minWeightThis;
        maxWeightThis = max(maxWeight, min(aveWeight * sqrt(5), maxWeightThis));
        bounds(1, col) = numToAllocateEq;
        bounds(2,col) = aveWeight;
        bounds(3,col) = minWeightThis;
        bounds(4,col) = maxWeightThis;      
        
        lb(ixEq) = minWeightThis;
        ub(ixEq) =  maxWeightThis;
        
        % Bounds on Fixed-Income & Cash instruments ----------------
        weightToAllocate = 1 - equityWeightVec(col);
        ixFi = indicatorVec & isEquity == 0;
        numToAllocate = sum(ixFi);
        aveWeight = weightToAllocate / numToAllocate;
        aveWeight(isnan(aveWeight)) = 0;
        minWeightThis = min(minWeight, aveWeight / sqrt(5));
        %minWeightThis = minWeight;
        maxWeightThis = 1.0 - (numToAllocate-1) * minWeightThis;
        maxWeightThis = max(maxWeight, min(aveWeight * sqrt(5), maxWeightThis));
        bounds(5, col) = numToAllocate;
        bounds(6,col) = aveWeight;
        bounds(7,col) = minWeightThis;
        bounds(8,col) = maxWeightThis;      
        
        lb(ixFi) = minWeightThis;
        ub(ixFi) =  maxWeightThis;
        
        
        sigmaTarget = sigmaTargetVec(col);
        [w, mu, sigma] = portOpt(RET.Mean(:), covMx, sigmaTarget, lb, ub, equityWeightVec(col), isEquity);

        riskWeightMx(:,col) = w;
        portReturn(col) = mu;
        portSigma(col) = sigma;
        
    end
end

figure; plt = plot(portSigma, portReturn, riskVecFront, returnVecFront, '-'); grid on;
        legend('Constrained Portfolios', 'Unconstrained Frontier', 'Location', 'NorthWest');
        xlabel('\sigma'); ylabel('Return'); xyzCursor(false); 
        plt(1).Marker = '.';
        
