function [digits, first_in_range] = calculate_range(num_to_convert)
    digits = 1;
    first_in_range = 0;
    current_sum = 26;
    while num_to_convert > current_sum
        digits = digits + 1;
        first_in_range = current_sum;
        current_sum = first_in_range + 26.^digits;
    end
end