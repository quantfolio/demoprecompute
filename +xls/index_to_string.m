function string = index_to_string(index, first_in_range, digits)
    letters = 'A':'Z';
    working_index = index - first_in_range;
    outputs = cell(1,digits);
    [outputs{1:digits}] = ind2sub(repmat(26,1,digits), working_index);
    string = fliplr(letters([outputs{:}]));
end
