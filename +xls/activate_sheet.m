function [theMessage, TargetSheet, visibility] = activate_sheet(Excel,Sheet)
    % Activate specified worksheet in workbook.
    % Initialize worksheet object
    WorkSheets = Excel.sheets;
    theMessage = struct('message',{''},'identifier',{''});
    
    % Get name of specified worksheet from workbook
    try
        TargetSheet = get(WorkSheets,'item',Sheet);
    catch exception  %#ok<NASGU>
        % Worksheet does not exist. Add worksheet.
        TargetSheet = xls.addsheet(WorkSheets,Sheet);
        warning(message('MATLAB:xlswrite:AddSheet'));
        if nargout > 0
            [theMessage.message,theMessage.identifier] = lastwarn;
        end
    end
    
    % We temporarily make xlSheetHidden and xlSheetVeryHidden spreadsheets
    % xlsheetVisible so we can write to them. Store the visibility for later.
    visibility = get(TargetSheet, 'Visible');
    
    % Activate silently fails if the sheet is hidden
    set(TargetSheet, 'Visible', 'xlSheetVisible');
    
    % activate worksheet
    Activate(TargetSheet);
end