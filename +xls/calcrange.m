function range = calcrange(range,m,n)
    % Calculate full target range, in Excel A1 notation, to include array of size
    % m x n
    
    range = upper(range);
    cols = isletter(range);
    rows = ~cols;
    % Construct first row.
    if ~any(rows)
        firstrow = 1; % Default row.
    else
        firstrow = str2double(range(rows)); % from range input.
    end
    % Construct first column.
    if ~any(cols)
        firstcol = 'A'; % Default column.
    else
        firstcol = range(cols); % from range input.
    end
    try
        lastrow = num2str(firstrow+m-1);   % Construct last row as a string.
        firstrow = num2str(firstrow);      % Convert first row to string image.
        lastcol = xls.dec2base27(xls.base27dec(firstcol)+n-1); % Construct last column.
        
        range = [firstcol firstrow ':' lastcol lastrow]; % Final range string.
    catch exception
        error(message('MATLAB:xlswrite:CalculateRange', range));
    end
end