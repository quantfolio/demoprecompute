function s = dec2base27(d)
    %   DEC2BASE27(D) returns the representation of D as a string in base 27,
    %   expressed as 'A'..'Z', 'AA','AB'...'AZ', and so on. Note, there is no zero
    %   digit, so strictly we have hybrid base26, base27 number system.  D must be a
    %   negative integer bigger than 0 and smaller than 2^52.
    %
    %   Examples
    %       dec2base(1) returns 'A'
    %       dec2base(26) returns 'Z'
    %       dec2base(27) returns 'AA'
    %-----------------------------------------------------------------------------
    d = d(:);
    if d ~= floor(d) || any(d(:) < 0) || any(d(:) > 1/eps)
        error('MATLAB:xlswrite:Dec2BaseInput',...
            'D must be an integer, 0 <= D <= 2^52.');
    end
    [num_digits, begin] = xls.calculate_range(d);
    s = xls.index_to_string(d, begin, num_digits);
end