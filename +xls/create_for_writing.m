function [Excel, Workbook] = create_for_writing(filename)
        %% Create Excel file for writing
        if isempty(filename)
            error('filename argument is not valid');
        end
                
        if exist(filename, 'file')
            delete(filename);
        end

        Excel = actxserver('Excel.Application');
        if ~exist(filename, 'file')
            Workbook = Excel.Workbooks.Add();
            Workbook.SaveAs(filename);
            Workbook.Close(false);
        end
        isReadOnly = false;
        Workbook = Excel.Workbooks.Open(filename, [], isReadOnly);
end