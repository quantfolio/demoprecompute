function resetVisibility(TargetSheet, visibility)
    % Rehide the sheet after writing if it was hidden at the start
    if ismember(visibility, {'xlSheetHidden', 'xlSheetVeryHidden'})
        set(TargetSheet, 'Visible', visibility);
    end
end