function messageStruct = exceptionHandler(nArgs, exception)
    if nArgs == 0
        throwAsCaller(exception);
    else
        messageStruct.message = exception.message;
        messageStruct.identifier = exception.identifier;
    end
end