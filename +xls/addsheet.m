function newsheet = addsheet(WorkSheets,Sheet)
    % Add new worksheet, Sheet into worksheet collection, WorkSheets.
    
    if isnumeric(Sheet)
        % iteratively add worksheet by index until number of sheets == Sheet.
        while WorkSheets.Count < Sheet
            % find last sheet in worksheet collection
            lastsheet = WorkSheets.Item(WorkSheets.Count);
            newsheet = WorkSheets.Add([],lastsheet);
        end
    else
        % add worksheet by name.
        % find last sheet in worksheet collection
        lastsheet = WorkSheets.Item(WorkSheets.Count);
        newsheet = WorkSheets.Add([],lastsheet);
    end
    % If Sheet is a string, rename new sheet to this string.
    if ischar(Sheet)
        set(newsheet,'Name',Sheet);
    end
end