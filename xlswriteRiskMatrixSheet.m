function [Excel, Workbook] = xlswriteRiskMatrixSheet(Excel, Workbook, sheetName, portTable, portFig)
    %function [Excel, Workbook] = xlswriteRiskMatrix(action, filename, Excel, Workbook, sheetName, portTable, portFig)
    % Do the interesting stuff: create, write, and format the new sheet
    
    ExecuteWriteTable();
    ExecuteWriteFig();
    
    
    %% Nested Functions =================================================================
    function ExecuteWriteFig()
        [~, TargetSheet, visibility] = xls.activate_sheet(Excel, sheetName);
        
        set(portFig,'PaperPositionMode','auto')
        fpos = get(portFig, 'Position');
        figHeight = fpos(4);
        print(portFig, '-clipboard', '-dbitmap', '-r0');
        strRange = 'U25';
        TargetSheet.Range(strRange).PasteSpecial;
        
        TargetSheet.Range('A1').Activate;
        xls.resetVisibility(TargetSheet, visibility);
    end
    
    
    function ExecuteWriteTable()
        [Nr, Nc] = size(portTable);
        sheetRange = xls.calcrange('B2', Nr, Nc);
        
        if Workbook.ReadOnly ~= 0
            %This means the file is probably open in another process.
            error('Workbok was opened in "ReadOnly" mode');
        end
        
        try
            [~, TargetSheet, visibility] = xls.activate_sheet(Excel, sheetName);
        catch
            error(message('MATLAB:xlswrite:InvalidSheetName', sheetName));
        end
        
        try
            Select(Range(Excel, sheetRange));
        catch
            error(message('MATLAB:xlswrite:SelectDataRange'));
        end
        
        % Export data to selected region.
        set(Excel.selection, 'Value', portTable);
        ApplyTableFormat();
        xls.resetVisibility(TargetSheet, visibility);
    end
    
    function ApplyTableFormat()
        Excel.Range('H4,K4,N4,Q4').Style = 'Good';
        Excel.Range('I4,L4,O4,R4').Style = 'Neutral';
        Excel.Range('J4,M4,P4,S4').Style = 'Bad';
        
        Excel.Range('B4:G4').Select;
        Excel.Selection.Font.Bold = true;
        Excel.Selection.WrapText = true;
        
        Excel.Range('E4:S4').Select;
        Excel.Selection.HorizontalAlignment = -4152;  % xlRight
        
        Excel.Range('B2,V2').Select;
        Excel.Selection.Font.Bold = true;
        Excel.Selection.Font.Size = 14;
        
        Excel.Range('G25:G32').Select;
        Excel.Selection.Font.Bold = true;
        Excel.Selection.HorizontalAlignment = -4152;  % xlRight
        
        Excel.ActiveWindow.DisplayGridlines = false;
        
        Excel.Range('H5:S23').Select;
        Excel.Selection.Borders(1).LineStyle = 1;  % xlContinuous
        Excel.Selection.Borders(1).Weight = 1;  % xlHairline
        
        Excel.Range('D5:D23').Font.Bold = true;
        Excel.Range('F5:G23').Style = 'Percent';
        Excel.Range('F5:G23').NumberFormat = '0.0%';
        
        Excel.Range('H25:S32').Select;
        Excel.Selection.Style = 'Percent';
        Excel.Selection.NumberFormat = '0.0%';
        
        Excel.Range('B:B').ColumnWidth = 14;
        Excel.Range('C:D').EntireColumn.AutoFit;
        Excel.Range('H:S').ColumnWidth = 6.14;
        
        
        % Format Correlation Matrix --------------------
        Excel.Range('V5:V23').Select;
        Excel.Selection.Font.Bold = true;
        Excel.Selection.HorizontalAlignment = -4152;  % xlRight
        
        Excel.Range('W4:AO4').Select;
        Excel.Selection.Font.Bold = true;
        Excel.Selection.HorizontalAlignment = -4152;  % xlRight
        
        Excel.Range('V:AO').ColumnWidth = 6.0;
        
        Excel.Range('W5:AO23').Select;
        Excel.Selection.Style = 'Comma';
        
        Excel.Range('W5,X6,Y7,Z8,AA9,AB10,AC11,AD12,AE13,AF14,AG15,AH16,AI17,AJ18,AK19,AL20,AM21,AN22,AO23').Select;
        Excel.Selection.NumberFormat = '_(* #,##0_);_(* (#,##0);_(* ""-""??_);_(@_)';
        
        
        % Draw borders and lines between asset classes ------------------------
        Excel.Range('B4:S4,W4:AO4').Select;
        border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('B7:S7,W7:AO7').Select;
        border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('B9:S9,W9:AO9').Select;
        border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('B14:S14,W14:AO14').Select;
        border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('B23:S23,W23:AO23').Select;
        border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('B5:B23,W5:W23').Select;
        border = Excel.Selection.Borders.Item(1);  % xlEdgeLeft
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        Excel.Range('S5:S23,AO5:AO23').Select;
        border = Excel.Selection.Borders.Item(2);  % xlEdgeRight
        border.LineStyle = 1;  % xlContinuous
        border.Weight = 2;  % xlThin
        
        xxx=0;  % for setting a breakpoint
        
    end
end