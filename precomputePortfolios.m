function [PCSET, symbolSpec] = precomputePortfolios(silentMode)

    % Produces a PCSET, which is a set of precomputed portfolios.  PCSET is a struct
    % with field names corresponding to the rebalance intervals that are supported (M, W or Y)
    % For each rebalance interval, a BTSET is created.  Each BTSET is a struct with 
    % contains a backtest for each single-group Selection Strategy.  The intention
    % is to use these to create blended portfolios by weighting, averaging, and rebalancing 
    % the single-group backtests in this PCSET
    %
    % git submodule update --recursive
    
    if ~exist('silentMode', 'var')  || isempty(silentMode)
        silentMode = false;
    end
    tStart = tic;
    
    TI=1; OP=2; HI=3; LO=4; CL=5; VO=6; TR=7; VA=8;  % last two are Total Return and Valuation

 
    %% Baseline param
    
    param = [];
    param.StrategyName = 'SB Precompute';
    param.Category = 'SB Precompute';
    param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
    param.SymbolSpecFile = 'Universe\1SB ETF and Fund QPM Master Mapping v1.4.csv';
    param.ResultsFile = 'Output\Results ROBO Precompute.xlsx';
    param.MarketDataCacheFile = 'InputCache\SB ROBO running.mat';
    param.CacheUpdateMode = 'AsNeeded';  % 'Offline', 'Force', 'AsNeeded'

    % TODO: Change this to the correct path
    param.LoadMorningstarData = true;

    param.StartDate = datenum(2013, 12, 31);
    param.EndDate = datenum(2017, 09, 30);

    param.StartCapital = 1000000;
    param.HomeCurrency = 'NOK';
    param.IndicatorCurrency = 'HOME';  % 'HOME', 'QUOTE'

%    param.ReallocatePeriod = 'Y'; % 'W', 'M', 'Q', 'S', 'Y', 'None'
    param.ReallocateDayOfWeek = '';  % 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'
    param.EquityCalculations = 'D'; % 'D', 'W', 'M'

    param.CommissionMinFixed = 0;
    param.CommissionMaxPct = 0.0;   % this is not in percent??
    param.CurrencySlippagePct = 0;

    param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
    param.DataGapsFillForward = 100;
    param.SilentMode = true;
    param.BenchmarkInstrument = '';
    
    param.QfPath = 'qs-matlab-engine';  % path containing code for QS Engine
    param.QpmPath = 'qpm-matlab-gui';
    addpath(param.QfPath, '-begin');
    addpath(param.QpmPath, '-begin');
    
    %%  Define Asset Classes as "groups" to use in strategy
    
    param = parseStratParam(param);
    [ROTATION, ~, ~, ~, ~] = loadMarketData(param);  % import ROTATION so we can compute indexes
    
    ixRobo = strcmpi(ROTATION.RoboEligible, 'yes');
    groupSpec = {};
    g = 0;
    
    g = g + 1;  
    groupSpec{g}.Name = 'c1';
    groupSpec{g}.AssetClassName = 'Cash';
    groupSpec{g}.Benchmark = 'FixedRate Cash 0';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c1') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';
    
    g = g + 1;
    groupSpec{g}.Name = 'c2';
    groupSpec{g}.AssetClassName = 'Money market';
    groupSpec{g}.Benchmark = 'ST1X Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c2') & ixRobo;
    groupSpec{g}.SelectionRule = 'BondETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c4';
    groupSpec{g}.AssetClassName = 'Corporate Bonds (investment grade)';
    groupSpec{g}.Benchmark = 'ST4X index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c4') & ixRobo;
    groupSpec{g}.SelectionRule = 'BondETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c23';
    groupSpec{g}.AssetClassName = 'Global bonds';
    groupSpec{g}.Benchmark = 'LGTRTRUU Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c23') & ixRobo;
    groupSpec{g}.SelectionRule = 'BondETF';    
        
    g = g + 1; 
    groupSpec{g}.Name = 'c21';
    groupSpec{g}.AssetClassName = 'Foreign government bond - LT';
    groupSpec{g}.Benchmark = 'LGCPTRUU Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c21') & ixRobo;
    groupSpec{g}.SelectionRule = 'BondETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c9';
    groupSpec{g}.AssetClassName = 'Global developed markets';
    groupSpec{g}.Benchmark = 'M1WO Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c9') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
                
    g = g + 1;
    groupSpec{g}.Name = 'c8';
    groupSpec{g}.AssetClassName = 'Global emerging markets';
    groupSpec{g}.Benchmark = 'MXEF Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c8') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c7';
    groupSpec{g}.AssetClassName = 'Nordic index';
    groupSpec{g}.Benchmark = 'MXND Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c7') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;   
    groupSpec{g}.Name = 'c6';
    groupSpec{g}.AssetClassName = 'Home market';
    groupSpec{g}.Benchmark = 'OSEBX Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c6') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
        
    g = g + 1;   
    groupSpec{g}.Name = 'c14';
    groupSpec{g}.AssetClassName = 'Global Small Cap Equity';
    groupSpec{g}.Benchmark = 'MXWOSC Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c14') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c11';
    groupSpec{g}.AssetClassName = 'Optional - USA';
    groupSpec{g}.Benchmark = 'SPX Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c11') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c13';
    groupSpec{g}.AssetClassName = 'Optional - Latin America';
    groupSpec{g}.Benchmark = 'MXLA Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c13') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c12';
    groupSpec{g}.AssetClassName = 'Optional - Asia/Pacific';
    groupSpec{g}.Benchmark = 'MXFEJ Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c12') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c10';
    groupSpec{g}.AssetClassName = 'Optional - Europe';
    groupSpec{g}.Benchmark = 'MXEU Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c10') & ixRobo;
    groupSpec{g}.SelectionRule = 'EquityETF';
    
    g = g + 1;
    groupSpec{g}.Name = 'c100';
    groupSpec{g}.AssetClassName = 'Optional - Automation & Robotics';
    groupSpec{g}.Benchmark = 'Synthetic {{''MXWO0IT Index''  0.7}  {''MXWO0IN Index'' 0.3}}';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c100') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';
    
    g = g + 1;
    groupSpec{g}.Name = 'c101';
    groupSpec{g}.AssetClassName = 'Optional - Digitalisation';
    groupSpec{g}.Benchmark = 'MXWO0IT Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c101') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';
    
    g = g + 1;
    groupSpec{g}.Name = 'c102';
    groupSpec{g}.AssetClassName = 'Optional - Ageing Population';
    groupSpec{g}.Benchmark = 'Synthetic {{''MXWO0HC Index''  0.6}  {''MXWO0FN Index''  0.4}}';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c102') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';
    
    g = g + 1;
    groupSpec{g}.Name = 'c103';
    groupSpec{g}.AssetClassName = 'Optional - Healthcare Innovation';
    groupSpec{g}.Benchmark = 'MXWO0HC Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c103') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';
    
    g = g + 1;
    groupSpec{g}.Name = 'c105';
    groupSpec{g}.AssetClassName = 'World Alternative Energy CW TR EUR';
    groupSpec{g}.Benchmark = 'WAEXC Index';
    groupSpec{g}.Ix = strcmpi(ROTATION.AssetClassCode, 'c105') & ixRobo;
    groupSpec{g}.SelectionRule = 'OneOption';    
    
    Ng = length(groupSpec);  % number of groups = asset classes
                
    
    %% Import all data into memory from disk & database - one group per benchmark 
                        
    bmSymbols = cell(1,Ng);
    for g = 1:Ng
        bmSymbols{g} = groupSpec{g}.Benchmark;  % collect BM symbols, so we are sure to get them in REFERENCE struct
    end
    bmSymbols = unique(bmSymbols(~cellisempty(bmSymbols)));
    for g = 1:length(bmSymbols)
        param.Selection.Group{g}.Benchmark = bmSymbols{g};
        param.Selection.Group{g}.Weight = 1/length(bmSymbols);  % Dummy weight, doesn't matter
    end
    param.BenchmarkInstrument = 'Synthetic';

    [ROTATION, TIMING, REFERENCE, CCY, symbolSpec] = loadMarketData(param);
    
    fprintf('Market Data has been loaded, tElapsed = %1.1f sec\n', toc(tStart));

    %% Iterate over the portfolios, collect results in an output file
    
    
    % Pre-Compute barNumSlice and pass to backtester to avoid recomputing
    btDates = fix(param.StartDate):(fix(param.EndDate) + param.DataGapsFillForward);
    barNumSlices = nan(length(btDates), length(ROTATION.Symbol));
    for m = 1:length(btDates)
        barNumSlices(m,:) = getBarNumSlice(ROTATION.Bars, btDates(m));
    end
    
    reallocPeriods = {'Q', 'Y'};
    
    PCSET = struct;  % a set of BTSETS, each with a differen reallocation period
    REPORT = struct;
    for p = 1:length(reallocPeriods)
    
        param.ReallocatePeriod = reallocPeriods{p};
        param = rmfield(param, 'Selection');  % remove it then add a new one each time
        
        BTSET = struct;
        for g = 1:length(groupSpec)
            param.Selection.Group{1}.Name = groupSpec{g}.Name;
            param.Selection.Group{1}.AssetClassName = groupSpec{g}.AssetClassName;
            param.Selection.Group{1}.Benchmark = groupSpec{g}.Benchmark;
            param.Selection.Group{1}.Ix = groupSpec{g}.Ix;
            param.Selection.Group{1}.SelectionRule = groupSpec{g}.SelectionRule;
            param.Selection.Group{1}.Formula = 'no formula';
            param.Selection.Group{1}.Weight = 1;            

            [state, bt, perf, ~, ...
             ~, ~, ~, ~, ~] = RunStrategyBacktest(param, symbolSpec, ROTATION, TIMING, REFERENCE, CCY, btDates, barNumSlices);

            BTSET.Param{g,1} = param;
            BTSET.RunDate{g,1} = bt.runDate;
            BTSET.AllocDates{g,1} = bt.allocDates;
            BTSET.Equity{g,1}  = perf.Equity;
            BTSET.PortWeights{g,1} = state.portWeights;
            BTSET.Symbol{g,1} = state.Symbol;
            
            REPORT.(param.ReallocatePeriod){g,1} = selectionReport(param, symbolSpec, state);  
        end
        
        PCSET.(param.ReallocatePeriod) = BTSET;
    end
    
    fprintf('Selection Engine is complete, tElapsed = %1.1f sec\n', toc(tStart));
    
    %% Create risk matrix, with 4 investmentHorizons and 3 riskTolerances per investmentHorizon
    
    [RiskMatrixStruct, RiskSymbolSpec, RiskMetaData, RiskParam, ...
        CellArrayOfBars, CellArrayOfBarsQuoteCurrency, CellArrayOfBarsHomeCurrency, success] = precomputeRiskMatrix(silentMode);

    if success
        PCSET.Risk.Param = RiskParam;
        PCSET.Risk.RiskMatrixStruct = RiskMatrixStruct;
        PCSET.Risk.RiskSymbolSpec = RiskSymbolSpec;
        PCSET.Risk.RiskMetaData = RiskMetaData;
        PCSET.Risk.CellArrayOfBars = CellArrayOfBars;
        PCSET.Risk.CellArrayOfBarsQuoteCurrency = CellArrayOfBarsQuoteCurrency;
        PCSET.Risk.CellArrayOfBarsHomeCurrency = CellArrayOfBarsHomeCurrency;
    else
        error('Unable to compute Risk Weight Matrix.  Please examine the optimization and contraints in "precomputeRiskMatrix.m"');
    end
    
    fprintf('RiskMatrix is complete, tElapsed = %1.1f sec\n', toc(tStart));


    %% Precompute Full-length Backtests for each AssetClass, using
    %  the benchmarks that produced the RiskMatrix
    
    mgmtFee = 0.40; % 40 basis points, in percent
       
    Nrm = length(PCSET.Risk.RiskMatrixStruct.RiskMatrix);  % number of RiskMatrix matrices, each corresponding to a subset of optional asset classes

    % BarSet = PCSET.Risk.CellArrayOfBarsHomeCurrency;
    BarSet = PCSET.Risk.CellArrayOfBars;  % Most in QuoteCurrency, Nordics is in HomeCurrency
    Nac = length(BarSet);
    maxDate = 0;
    for m = 1:length(BarSet)
        maxDate = max(maxDate, BarSet{m}(end,TI));
    end    
    
    startDate = PCSET.Risk.Param.StartDate;  % use entire RiskMatrix history, plus more recent data
    eomDate = eomdate(maxDate);
    wkd = weekday(eomDate);  
    if wkd == WeekDays.Saturday  
        eomTradeDate = eomDate - 1;
    elseif wkd == WeekDays.Sunday  
        eomTradeDate = eomDate - 2;
    else
        eomTradeDate = eomDate;
    end
    if maxDate >= eomTradeDate  % if we have data covering the last trade date of this month
        endDate = maxDate;  % end of this month, even if it's in the future
    else
        endDate = datenum(year(maxDate), month(maxDate), 0);  % end of previous month
    end
    
    
    % For MonteCarlo simulation:
    Nyears = 25;  % maximum investment horizon used by QPM
    Nrealizations = 1000;
    forecastInterval = 'M';
    doPlots = false;
    
    for p = 1:length(reallocPeriods)
        reallocPeriod = reallocPeriods{p};
        allocDates = generateRunDates(reallocPeriod, startDate, endDate, []);
        allocDates = allocDates(:);

        btDates = allocDates(:);  % find backtest dates, as union of allocDates and dates with nonzero returns
        for s = 1:Nac
            dates = BarSet{s}(:,TI);
            prices = BarSet{s}(:,VA); 
            returns = [0; prices(2:end)./prices(1:end-1)-1]; 
            ix = (returns ~= 0) & dates >= startDate & dates <= endDate;
            btDates = unique([btDates; dates(ix)]);
        end
        
        % Precompute Per-AssetClass equity curves --------------------------        
        strLegend = cell(Nac, 1);
        eqMx = nan(length(btDates), Nac);
        for s = 1:Nac
            strLegend{s} = PCSET.Risk.RiskMetaData.AssetClassCode{s};
            [Lia, Locb] = ismember(BarSet{s}(:,TI), btDates);
            equity = nan(length(btDates), 1);
            equity(Locb(Lia)) = BarSet{s}(Lia,VA);

            m0 = find(~isnan(equity), 1, 'first');
            for m = m0+1:length(equity)  % fill-forward missing values
                if isnan(equity(m))
                    equity(m) = equity(m-1);
                end
            end
            eqMx(:,s) = equity / equity(m0);
        end
        ixM = downsampleMonthly(btDates);
        PCSET.Backtest.(reallocPeriod).Dates = btDates(ixM);
        PCSET.Backtest.(reallocPeriod).EqMx = eqMx(ixM,:);
        PCSET.Backtest.(reallocPeriod).StrLegend = strLegend;
                
        % Precompute Backtest and Monte Carlo Forecast equity curves for all subsets of optionals ------------------
        for riskMatrixIx = 1:Nrm
            riskMatrix = PCSET.Risk.RiskMatrixStruct.RiskMatrix{riskMatrixIx};
            [~, Np] = size(riskMatrix);
            for riskMatrixCol = 1:Np
                weights = PCSET.Risk.RiskMatrixStruct.RiskMatrix{riskMatrixIx}(:, riskMatrixCol);        
                btEquity = blendWeightRebal(BarSet, weights, mgmtFee, allocDates, btDates);
                PCSET.Backtest.(reallocPeriod).Equity{riskMatrixIx, riskMatrixCol} = btEquity(ixM);
                
                mc = monteCarloEquityProjectionQPM(btDates, btEquity, endDate, Nyears, Nrealizations, forecastInterval, doPlots);
                mc = rmfield(mc, 'eqSimMx');
                PCSET.Forecast.(reallocPeriod).MC{riskMatrixIx, riskMatrixCol} = mc;
            end
            fprintf('Sims for RiskMatrix %d/%d, Rebal = %s, ElapsedTime = %1.2f sec\n', riskMatrixIx, Nrm, reallocPeriod, toc(tStart));
        end
    end
    
    fprintf('Full Backtests and Forecasts are complete, tElapsed = %1.1f sec\n', toc(tStart));
    
    %% Write output
    
    PCSET.ROTATION = ROTATION;
    PCSET.REFERENCE = REFERENCE;
    PCSET.Timestamp = datetime('now','TimeZone', 'UTC');
    
    if ~silentMode    
        outPath = fullfile(pwd, 'Output');
        filename = sprintf('Precompute_%s.mat', datestr(PCSET.Timestamp, 'yyyy-mm-dd_HHMMSS'));
        filename = fullfile(outPath, filename);
        if ~exist(outPath, 'dir')
            mkdir(outPath);
        end
        save(filename, 'PCSET', 'symbolSpec', '-v7')
        fprintf('Data has been written, tElapsed = %1.1f sec\n', toc(tStart)); 
        
        xlswriteSelectionReport(outPath, PCSET, REPORT, groupSpec, reallocPeriods);
        xlswriteAllFundsRatingReport(outPath, PCSET, symbolSpec)
    end
            
end