function tests = test_dataset()
    path('qs-matlab-engine', path)
    tests = functiontests(localfunctions);
end

function test_AllInstrumentsMustHaveBarsData(testCase)
    ROTATION = testCase.TestData.ROTATION;
    Bars = ROTATION.Bars;
    
    ixNoReturns = find(cellfun(@isempty, Bars));
    if ~isempty(ixNoReturns)
         fail(testCase, ROTATION, ixNoReturns); 
    end
end

function test_BarsDataTooOld(testCase)
    ROTATION = testCase.TestData.ROTATION;
    inactiveStatuses = {'LIQU','ACQU', 'DLST', 'INAC', 'EXPD' };
    
    ixHasReturns = find(~cellfun(@isempty,  ROTATION.Bars));
    indexes = IsDataTooOld(ROTATION.Bars, ixHasReturns, ROTATION.EndDate(end));
    inactiveFunds = ismember(ROTATION.MARKET_STATUS(indexes), inactiveStatuses);
    
    ixTooOldData = indexes(~inactiveFunds);
    if ~isempty(ixTooOldData)
        fail(testCase, ROTATION, ixTooOldData); 
    end
end

function test_AllRobofundsMustBeActive(testCase)
    ROTATION = testCase.TestData.ROTATION;
    ixRobo = testCase.TestData.ixRobo;
    
    statuses = ROTATION.MARKET_STATUS(ixRobo);
    ixInactiveRoboFund = find(~ismember(statuses,'ACTV'));
    if ~isempty(ixInactiveRoboFund)
        fail(testCase, ROTATION, ixInactiveRoboFund); 
    end
end


function test_AllFundsMustHaveFIGI(testCase)
    ROTATION = testCase.TestData.ROTATION;
    
    ixCash = find(strcmp(ROTATION.Symbol,'FixedRate Cash 0'));
    ixNoFIGI = find(cellfun(@isempty, ROTATION.FIGI));
    ixNoFIGI = setdiff(ixNoFIGI, ixCash);
    if ~isempty(ixNoFIGI)
         fail(testCase, ROTATION, ixNoFIGI); 
    end
end

function test_BenchMarkFundsMustHaveBenchMarks(testCase)
    ROTATION = testCase.TestData.ROTATION;
    ixRoboData = testCase.TestData.ixRobo;
    
    assetClassList = {'c2','c4','c6', 'c7', 'c8', 'c9','c10','c11','c12','c13','c14','c21','c23'};
    
    ixInAssetClassList= find(ismember(ROTATION.AssetClassCode, assetClassList));
    ixNeedsBenchmark = intersect(ixRoboData, ixInAssetClassList);
    bbBenchmark = ROTATION.BBBenchmark(ixNeedsBenchmark);
    ixEmptyBBBenchmark = find(cellfun(@isempty,bbBenchmark));
    indexes = ixNeedsBenchmark(ixEmptyBBBenchmark);
    if ~isempty(indexes)
       fail(testCase, ROTATION, indexes); 
    end
end

function test_C4FundsMustHaveOADData(testCase)
    ROTATION = testCase.TestData.ROTATION;
    ixRoboData = testCase.TestData.ixRobo;
    
    ixAssetClassC4 = find(ismember(ROTATION.AssetClassCode,'c4'));
    ixNeedsOAD = intersect(ixAssetClassC4, ixRoboData);
    data = ROTATION.OAD(ixNeedsOAD);
    
    noOAD = find(cellfun(@isempty,data));
    if ~isempty(noOAD)
          fail(testCase, ROTATION, noOAD); 
    end
end

function test_C4FundsOADDataTooOld(testCase)
    ROTATION = testCase.TestData.ROTATION;
    ixRoboData = testCase.TestData.ixRobo;
    
    ixAssetClassC4 = find(ismember(ROTATION.AssetClassCode,'c4'));
    ixNeedsOAD = intersect(ixAssetClassC4, ixRoboData);
    
    [indexes] = IsDataTooOld(ROTATION.OAD, ixNeedsOAD, ROTATION.EndDate(end));
    if ~isempty(indexes)
         fail(testCase, ROTATION, indexes); 
    end
end

function test_ExpenseRatioDataExists(testCase)
    ROTATION = testCase.TestData.ROTATION;
    assetClassList = {'c2','c4','c6', 'c7', 'c8', 'c9','c10','c11','c12','c13','c14','c21','c23'};
    ixInAssetClassList= find(ismember(ROTATION.AssetClassCode, assetClassList));
    ixRoboData = testCase.TestData.ixRobo;
    
    ixNeedsExpenseRatioData = intersect(ixRoboData, ixInAssetClassList);
    
    expenseRatio = ROTATION.Q_ExpenseRatio(ixNeedsExpenseRatioData);
    ixEmptyExpanseRatio = cellfun(@isempty, expenseRatio);
    indexes = ixNeedsExpenseRatioData(ixEmptyExpanseRatio);
    if ~isempty(indexes)
         fail(testCase, ROTATION, indexes); 
    end
end

function test_ExpenseRatioDataTooOld(testCase)
    ROTATION = testCase.TestData.ROTATION;
    assetClassList = {'c2','c4','c6', 'c7', 'c8', 'c9','c10','c11','c12','c13','c14','c21','c23'};
    ixInAssetClassList= find(ismember(ROTATION.AssetClassCode, assetClassList));
    ixRoboData = testCase.TestData.ixRobo;
    ixNeedsExpenseRatioData = intersect(ixRoboData, ixInAssetClassList);
    
    [indexes] = IsDataTooOld(ROTATION.Q_ExpenseRatio, ixNeedsExpenseRatioData, ROTATION.EndDate(end));
    if ~isempty(indexes)
        fail(testCase, ROTATION, indexes); 
    end
end

function test_FundSizeDataExists(testCase)
    ROTATION = testCase.TestData.ROTATION;
    assetClassList = {'c6', 'c7', 'c8', 'c9','c10','c11','c12','c13','c14'};
    ixInAssetClassList= find(ismember(ROTATION.AssetClassCode, assetClassList));
    ixRoboData = testCase.TestData.ixRobo;
    
    ixNeedsFundSizeData = intersect(ixRoboData, ixInAssetClassList);
    
    fundSizeData = ROTATION.FundSize(ixNeedsFundSizeData);
    ixEmptyFundSizeData = find(cellfun(@isempty, fundSizeData));
    if ~isempty(ixEmptyFundSizeData)
         fail(testCase, ROTATION, ixEmptyFundSizeData); 
    end
end

function test_FundSizeDataTooOld(testCase)
    ROTATION = testCase.TestData.ROTATION;
    assetClassList = {'c6', 'c7', 'c8', 'c9','c10','c11','c12','c13','c14'};
    ixInAssetClassList= find(ismember(ROTATION.AssetClassCode, assetClassList));
    ixRoboData = testCase.TestData.ixRobo;
    ixNeedsFundSizeData = intersect(ixRoboData, ixInAssetClassList);
    
    [indexes] = IsDataTooOld(ROTATION.FundSize, ixNeedsFundSizeData, ROTATION.EndDate(end));
    if ~isempty(indexes)
        fail(testCase, ROTATION, indexes); 
    end
end


function test_NoGapsInData(testCase)
    ROTATION = testCase.TestData.ROTATION;
    Bars = testCase.TestData.ROTATION.Bars;
    
    % loop over bars data and find any with gaps > 30 days
    ixWithGaps = [];
    for i=1:length(Bars)
        bar = Bars{i};
        if isempty(bar)
            continue;
        end
        if max(diff(bar(:,1))) > 30
            ixWithGaps = [ixWithGaps; i]; %#ok<AGROW>
        end
    end
    if ~isempty(ixWithGaps)
        fail(testCase, ROTATION, ixWithGaps); 
    end
end

function fail(testCase, ROTATION, indexes)
    symbolNames = ROTATION.Symbol(indexes);
    perfIds = ROTATION.PerformanceId(indexes);
    cellStr = [perfIds;symbolNames];
    testCase.verifyFail(sprintf('%10s - %s\n', cellStr{:} ))
end

function [indexes] = IsDataTooOld(data, ix, endDate)
    [~,~,d] = datevec(endDate);
    minDate = addtodate(endDate,-1,'month')-d;
    indexes = [];
    for i=ix
        ts = data{i};
        if ~isempty(ts)
            if ts(end,1) < minDate
                indexes = [indexes; i];
            end
        end
    end
end

function setupOnce(testCase)
    param = [];
    param.StrategyName = 'SB Precompute';
    param.Category = 'SB Precompute';
    param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
    param.SymbolSpecFile = 'Universe\1SB ETF and Fund QPM Master Mapping v1.4.csv';
    param.ResultsFile = 'Output\Results ROBO Precompute.xlsx';
    param.MarketDataCacheFile = 'InputCache\SB ROBO running.mat';
    param.CacheUpdateMode = 'Offline';  % 'Offline', 'Force', 'AsNeeded'
    
    param.LoadMorningstarData = true;
    
    param.StartDate = datenum(2013, 12, 31);
    param.EndDate = datenum(2017, 08, 01);
    
    param.StartCapital = 1000000;
    param.HomeCurrency = 'NOK';
    param.IndicatorCurrency = 'HOME';  % 'HOME', 'QUOTE'
    
    param.EquityCalculations = 'D'; % 'D', 'W', 'M'
    
    param.CommissionMinFixed = 0;
    param.CommissionMaxPct = 0.0;   % this is not in percent??
    param.CurrencySlippagePct = 0;
    
    param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
    param.DataGapsFillForward = 100;
    param.SilentMode = true;
    param.BenchmarkInstrument = '';
    
    param.QfPath = '..';  % path containing code for QS Engine
    path(param.QfPath, path);
    
    ROTATION = loadMarketData(param);
    ixFixedCash = find(strcmp(ROTATION.PerformanceId,'FixedRate Cash 0'));
    testCase.TestData.ROTATION = ROTATION;
    testCase.TestData.ixRobo = setdiff(find(strcmp(ROTATION.RoboEligible, 'yes')), ixFixedCash);
end