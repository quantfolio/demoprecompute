function report = selectionReport(param, symbolSpec, state)

    Nd = length(state.allocationDateStr);
    Ng = length(param.Selection.Group);
    
    rowsPerDate = 0;
    for g = 1:Ng
        Ns = sum(param.Selection.Group{g}.Ix);
        rowsPerDate = rowsPerDate + Ns;
    end
    
    header = {'Date', 'AssetClassCode', 'Symbol', 'InstrumentName', 'Weight', 'ExpenseRatioDate', ...
              'ExpenseRatio', 'TrackingError', 'Cost', 'AnnualizedReturn', 'CostAdjustedReturn', 'ReturnYears', ...
              'FundSizeDate', 'FundSize', 'Duration', 'BenchmarkSymbol', 'BenchmarkName', 'BenchmarkYears', 'Reason'};
    
    Ncol = length(header);
    report = cell(1 + Nd * rowsPerDate, Ncol);
    report(1,:) = header;
    
    for d = 1:Nd  % for each date
        for g = 1:Ng
            ix = find(param.Selection.Group{g}.Ix);            
            Ns = length(ix);
            
            reportThisDate = cell(Ns, Ncol);
            for s = 1:Ns  % for each symbol
                % make sure param.Selection.Group is in sync with state.group
                if ~strcmpi(state.group{g}.Symbol{s}, symbolSpec.Symbol{ix(s)})
                    error('Expected symbols to match');
                end
                reportThisDate{s, 1} = state.allocationDateStr{d};
                reportThisDate{s, 2} = param.Selection.Group{g}.Name;
                reportThisDate{s, 3} = symbolSpec.Symbol{ix(s)};
                reportThisDate{s, 4} = symbolSpec.InstrumentName{ix(s)};
                reportThisDate{s, 5} = state.portWeights(d, ix(s));                
                dateVal = state.group{g}.ExpenseRatioDate(d, s);
                if isnan(dateVal)
                    dateStr = '';
                else
                    dateStr = datestr(dateVal, 'yyyy-mm-dd');
                end
                reportThisDate{s, 6}  = dateStr;
                reportThisDate{s, 7}  = state.group{g}.ExpenseRatio(d, s);
                reportThisDate{s, 8}  = state.group{g}.ExcessReturn(d, s);
                reportThisDate{s, 9} = state.group{g}.Cost(d, s);
                reportThisDate{s, 10} = state.group{g}.AnnualizedReturn(d, s);
                reportThisDate{s, 11} = state.group{g}.CostAdjustedReturn(d, s);
                reportThisDate{s, 12}  = state.group{g}.ReturnYears(d, s);                
                dateVal = state.group{g}.FundSizeDate(d, s);
                if isnan(dateVal)
                    dateStr = '';
                else
                    dateStr = datestr(dateVal, 'yyyy-mm-dd');
                end
                reportThisDate{s, 13}  = dateStr;                
                reportThisDate{s, 14} = state.group{g}.FundSize(d, s);    
                reportThisDate{s, 15} = state.group{g}.Duration(d, s);
                reportThisDate{s, 16} = state.group{g}.BenchmarkSymbol{d, s};
                reportThisDate{s, 17} = state.group{g}.BenchmarkName{d, s};
                reportThisDate{s, 18} = state.group{g}.BenchmarkYears(d, s);
            end
            ixW = find(cell2mat(reportThisDate(:,5)) > 0, 1, 'first');  % expect only one instrument with non-zero weight
            reportThisDate(ixW, 19) = state.group{g}.Reason(d);
            
            % Now sort the report by the applicable selection metrics
            if strcmpi(param.Selection.Group{g}.SelectionRule, 'EquityETF')
                % Sort by: Cost (acending), FundSize (decending), AnnualizedReturn (descending)
                reportThisDate = sortrows(reportThisDate, [9, 14, 10], {'ascend', 'descend', 'descend'});
            elseif strcmpi(param.Selection.Group{g}.SelectionRule, 'BondETF')
                % Sort by: CostAdjustedReturn (descending), Symbol alphabetically (ascending)
                reportThisDate = sortrows(reportThisDate, [11, 3], {'descend', 'ascend'});                
            end
                       
            % copy today's data into the larger report
            % make sure the "Reason" row appears first (rows with empty selection metrics may have sorted before it)
            ix1 = find(~cellisempty(reportThisDate(:,end)), 1, 'first');  % row containing the Reason
            ixR = 1 + (d-1) * rowsPerDate + (1:Ns);
            report(ixR,:) = [reportThisDate(ix1:end,:); reportThisDate(1:ix1-1, :)];  
        end 
    end
end