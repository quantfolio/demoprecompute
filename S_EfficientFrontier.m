
param = [];
param.StrategyName = 'SB Markowitz';
param.Category = 'SkandiaBanken';
param.Type = 'Selection';    % 'Portfolio', 'Factor', 'Trend', 'Selection'
param.SymbolSpecFile = 'Universe\SB ROBO Universe 143 mapped.csv';
param.ResultsFile = 'Output\Results SB EfficientFrontier.xlsx';
param.MarketDataCacheFile = 'InputCache\SB ROBO running.mat';
param.CacheUpdateMode = 'Offline';  % 'Offline', 'Force', 'AsNeeded'

param.MorningstarRatingPath = '.\Morningstar\Rating';
param.ExpenseRatioPath = '.\Morningstar\Net_Exp_Ratio';

param.StartDate = datenum(2013, 12, 31);
param.EndDate = datenum(2016, 12, 31);

param.StartCapital = 1000000;
param.HomeCurrency = 'NOK';
param.IndicatorCurrency = 'HOME';  % 'HOME', 'QUOTE'

param.ReallocatePeriod = 'Y'; % 'W', 'M', 'Q', 'S', 'Y', 'None'
param.ReallocateDayOfWeek = '';  % 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'
param.EquityCalculations = 'D'; % 'D', 'W', 'M'

param.CommissionMinFixed = 0;
param.CommissionMaxPct = 0.0;   % this is not in percent??
param.CurrencySlippagePct = 0;

param.ValuationField = 'TOT_RETURN_INDEX_GROSS_DVDS';
param.DataGapsFillForward = 100;
param.SilentMode = true;
param.BenchmarkInstrument = '';

param.QfPath = '..';  % path containing code for QS Engine
path(param.QfPath, path);

%%

param = parseStratParam(param);

[ROTATION, TIMING, REFERENCE, CCY, symbolSpec] = loadMarketData(param);

TI=1; OP=2; HI=3; LO=4; CL=5; VO=6; TR=7; VA=8;  % last two are Total Return and Valuation

%%

Ns = length(ROTATION.Symbol);
for m = 1:length(ROTATION.Symbol)
    dates0 = ROTATION.Bars{m}(:,TI);
    ix = dates0 >= param.StartDate & dates0 <= param.EndDate;
    dates = dates0(ix);
    prices = ROTATION.BarsHomeCurrency{m}(ix, VA);
    nPerYear = length(dates) / ((dates(end) - dates(1)) / 365);
    logret = diff(log(prices));
    RET.Mean(m) = mean(logret) * nPerYear;
    RET.StdDev(m) = std(logret) * sqrt(nPerYear);
    
    ix = logret < 0;
    if sum(ix) > 0
        downsideLogret = zeros(size(logret));
        downsideLogret(ix) = logret(ix);
        RET.DownsideDev(m) = sqrt(sum(downsideLogret .^ 2) / length(downsideLogret)) * sqrt(252);
    else
        RET.DownsideDev(m) = 0;
    end
end

cellarrayofbars = ROTATION.BarsHomeCurrency;
[covMx, corMx, numObs, sigmaVec] = monthlyCov(cellarrayofbars, VA, param.StartDate, param.EndDate);

figure; plot(1:Ns, RET.StdDev, 1:Ns, sigmaVec); xlabel('Instrument'); ylabel('\sigma'); 
        grid on; legend('Daily (or max available)', 'Monthly');
        title('Sigma of Returns, computed two ways');

figure; plot(1:Ns, RET.Mean, 1:Ns, sigmaVec, 1:Ns, RET.StdDev); xlabel('Instrument'); grid on; 
        legend('Mean Return', 'StdDev Return (Monthly)', 'StdDev Return (Daily)');
        
%% A single target portfolio

sigmaTarget = .10;
tic;
[w, mu, sigma] = portOpt(RET.Mean(:), covMx, sigmaTarget, [], [], [], [], []);
toc

        
figure; plot(sigmaVec, RET.Mean, '.', sigma, mu, 'ro'); grid on; xlabel('\sigma'); ylabel('Return');
        legend('Instruments', 'Optimal Portfolio');

%% Efficient frontier

tic;
riskVecTarg = (0.01:.01:.25)';
weightMx = zeros(Ns, length(riskVecTarg));
returnVecFront = zeros(size(riskVecTarg));
riskVecFront = zeros(size(riskVecTarg));
for r = 1:length(riskVecTarg)
    [w, mu, sigma] = portOpt(RET.Mean(:), covMx, riskVecTarg(r), [], [], [], [], []);
    weightMx(:,r) = w;   
    returnVecFront(r) = mu;
    riskVecFront(r) = sigma;
end
toc

sharpeVecFront = returnVecFront ./ riskVecFront;

figure; subplot(2,1,1);
        plot(sigmaVec, RET.Mean, '.', riskVecFront, returnVecFront, '-'); grid on; 
        xlabel('\sigma'); ylabel('Return'); 
        legend('Instruments', 'Efficient Frontier', 'Location', 'SouthEast');

        subplot(2,1,2);
        plot(riskVecFront, sharpeVecFront); grid on; 
        xlabel('\sigma'); ylabel('Return/Risk Ratio');


