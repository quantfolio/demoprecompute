function [Excel, Workbook] = xlswriteAnalyzePortfolio(Excel, Workbook, sheetName, portTable, portFig1, portFig2, portFig3)
   
   % Do the interesting stuff: create, write, and format the new sheet
   ExecuteWriteTable();
   ExecuteWriteFig(portFig1, 'S5');
   ExecuteWriteFig(portFig2, 'S28');
   ExecuteWriteFig(portFig3, 'S50');
   
   %% Nested Functions =================================================================
   
   
   function ExecuteWriteFig(thisFig, strRange)
      if isempty(thisFig) || isnan(thisFig)
         return;
      end
      [~, TargetSheet, visibility] = activate_sheet(Excel, sheetName);
      
      set(thisFig,'PaperPositionMode','auto')
      fpos = get(thisFig, 'Position');
      figHeight = fpos(4);
      print(thisFig, '-clipboard', '-dbitmap', '-r0');
      TargetSheet.Range(strRange).PasteSpecial;
      Excel.Selection.ShapeRange.IncrementLeft(2.25);  % in pixels
      
      TargetSheet.Range('A1').Activate;
      resetVisibility(TargetSheet, visibility);
   end
   
   
   function ExecuteWriteTable()
      [Nr, Nc] = size(portTable);
      sheetRange = xls.calcrange('B2', Nr, Nc);
      
      if Workbook.ReadOnly ~= 0
         %This means the file is probably open in another process.
         error('Workbok was opened in "ReadOnly" mode');
      end
      
      try
         [~, TargetSheet, visibility] = xls.activate_sheet(Excel, sheetName);
      catch
         error(message('MATLAB:xlswrite:InvalidSheetName', sheetName));
      end
      
      try
         Select(Range(Excel, sheetRange));
      catch
         error(message('MATLAB:xlswrite:SelectDataRange'));
      end
      
      % Export data to selected region.
      set(Excel.selection, 'Value', portTable);
      ApplyTableFormat();
      xls.resetVisibility(TargetSheet, visibility);
   end
   
   
   function ApplyTableFormat()
      [Nr, ~] = size(portTable);
      Excel.Range('A:A').ColumnWidth = 2;
      Excel.Range('B2:W3').Font.Bold = true;
      Excel.Range('B3:I3').WrapText = true;
      Excel.Range('V3:W3').WrapText = true;
      Excel.Range('J3:T3,V3:W3').HorizontalAlignment = -4152;  % xlRight
      Excel.Range('D:E').HorizontalAlignment = -4108;  % xlCenter
      Excel.ActiveWindow.DisplayGridlines = false;
      
      rng = xls.calcrange('G4', Nr, 1);
      Excel.Range(rng).Style = 'Percent';
      Excel.Range(rng).NumberFormat = '0.00%';
      
      rng = xls.calcrange('J4', Nr, 1);
      Excel.Range(rng).NumberFormat = '0.0';
      rng = xls.calcrange('V4', Nr, 2);
      Excel.Range(rng).NumberFormat = '0.0';
      
      rng = xls.calcrange('K4', Nr, 10);
      Excel.Range(rng).Style = 'Percent';
      Excel.Range(rng).NumberFormat = '0.0%';
      Excel.Range('K:T').ColumnWidth = 6.43;
      
      Excel.Range('B:J').EntireColumn.AutoFit;
      Excel.Range('O:O,T:W').EntireColumn.AutoFit;
      Excel.Range('3:3').EntireRow.AutoFit;
      
      % Draw borders and lines between asset classes ------------------------
      Excel.Range('B3:W3').Select;
      border = Excel.Selection.Borders.Item(4);  % xlEddgeBottom
      border.LineStyle = 1;  % xlContinuous
      border.Weight = 2;  % xlThin
      
      startCells = {'F3', 'J3', 'K3', 'P3', 'U3', 'V3'};
      rng = xls.calcrange(startCells{1}, Nr, 1);
      for m = 2:length(startCells)
         rng = sprintf('%s,%s', rng, xls.calcrange(startCells{m}, Nr, 1));
      end
      
      Excel.Range(rng).Select;
      border = Excel.Selection.Borders.Item(1);  % xlEdgeLeft
      border.LineStyle = 1;  % xlContinuous
      border.Weight = 2;  % xlThin
      
      Excel.Range('A1').Select;
   end
end