function [w, mu, sigma] = portOpt(retVec, covMx, sigmaTarget, lb, ub, equityMin, equityMax, isEquity)
% retVec: Nx1 column vector containing expected annualized log returns of each instrument
% covMx:  NxN covariance matrix of log returns
% sigmaTarget: scalar maximum allowable standard deviation of annualized log returns
% lb: Nx1 column vector containing lower bound on weight allocated to each instrument
% ub: Nx1 column vector containing upper bound on weight allocated to each instrument
% equityWeight: scalar containing max allowable weight to allocated to equity instruments
% isEquity: Nx1 logical vector indicating which instruments are equity (vs fixed income, etc)

    Ns = length(retVec);
    
    % Upper and Lower bounds on weights of individual instruments in the optimization
    if ~exist('lb', 'var') || isempty(lb)
        lb = zeros(Ns,1);
    end
    if ~exist('ub', 'var') || isempty(ub)
        ub = ones(Ns,1);
    end
    
    if isempty(equityMin) && isempty(equityMax) || isempty(isEquity)
        A = [];
        b = [];
    else
        A = [-isEquity(:)'; ...
              isEquity(:)'];
        b = [-equityMin; equityMax];
    end
    

    Aeq = ones(1,Ns);  % weights sum to 1
    beq = 1;
    x0 = ones(Ns,1)/Ns;

    options = optimoptions('fmincon', 'MaxFunctionEvaluations', 5000, 'Display', 'off');
    w = fmincon(@objFun, x0,A,b,Aeq,beq,lb,ub, @nlcon, options);
    
    % Round the insignificant weights to 0, scale the rest to sum to 1.000
    ix = abs(w) < 0.01;
    wRound = sum(w(ix));
    w(ix) = 0;
    w(~ix) = w(~ix) / (1-wRound);
    
    mu = retVec' * w;
    sigma = sqrt(w' * covMx * w);


    function y = objFun(x)
        y = -retVec' * x;
    end
    
    function [c, ceq] = nlcon(x)
        c = x' * covMx * x - sigmaTarget ^ 2;  % require sigma <= target
        ceq = [];
    end    
    
end

